import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SwiperModule } from 'swiper/angular';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { SharedModule } from './shared/shared.module';
import { AngularMaterialModule } from './shared/angular-material/angular-material.module';
import { MenuService } from './core/service/menu/menu.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AvatarModule } from 'ngx-avatar';
import { ErrorInterceptor } from './core/interceptors/error.interceptor';
import { TokenInterceptor } from './core/interceptors/token.interceptor';
import { LandingModule } from './module/landing/landing.module';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { SpecialityModule } from './module/admin/speciality/speciality.module';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es-BO';
import { ShoppingCartModule } from './module/admin/shopping-cart/shopping-cart.module';
import { PixelModule } from 'ngx-pixel';
registerLocaleData(localeEs, 'es-BO');
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SwiperModule,
    SweetAlert2Module.forRoot(),
    SharedModule,
    AngularMaterialModule,
    AvatarModule,
    LandingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    SpecialityModule,
    ShoppingCartModule,
    PixelModule.forRoot({ enabled: true, pixelId: '1066072767420571' })
  ],
  providers: [
    MenuService,
    { provide: LOCALE_ID, useValue: 'es-BO' },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
