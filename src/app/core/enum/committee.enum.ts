export enum ECommittee {
  EDUCACION = 'Educación',
  COMUNICACION = 'Comunicación',
  SOCIAL = 'Social',
  AMBIENTAL = 'Ambiental',
}
