export enum ConstanstEnum {
  PATTERN_ONLY_TEXT = '^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$',
  PATTERN_ONLY_TEXT_NUMBER = '[A-Za-z0-9]*',
  DATE_FORMATE      = 'YYYY-MM-DD h:mm:ss',
  PATTERN_PHONE     = '^[6-7,4,5,3]+[0-9]+$'
}
