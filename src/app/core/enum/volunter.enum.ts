export enum ERolUser {
  PRESIDETE = 'Presidente',
  VICEPRESIDENTE = 'Vice Presidente',
  SECRETARIO = 'Secretario',
  TESORERO = 'Tesorero',
  RELACIONES = 'Relaciones Públicas',
}

export enum ECategoryUser {
  MESA = 'Mesa Directiva',
  COMITE = 'Comite',
}
