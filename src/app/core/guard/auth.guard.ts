import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { VariablesConstanst } from '../config/variables.config';
import { AuthConstans } from '../model/config/auth-constanst.config';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private cookieService: CookieService,
    private router: Router){}

    canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{

      const existToken = this.cookieService.check(AuthConstans.AUTH);
      if (!existToken) {
        this.router.navigate(['/login']);
        return false;
      }

      return true;
    }

}
