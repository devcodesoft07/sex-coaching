import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { ErrorService } from '../service/error/error.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(
        private errorService: ErrorService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req)
    .pipe(
      catchError( (error: HttpErrorResponse) => {
        let errorMessage: string = '';
        if (error.status === 0 && error.error instanceof ProgressEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          this.errorService.showMessageErrorClient(errorMessage, error.status);
        }
        if (error.error instanceof ErrorEvent) {
          errorMessage = error.message;
          this.errorService.showMessageErrorClient(errorMessage, error.status);
        } else {
          this.errorService.showMessageErrorBackend(errorMessage, error.status);
        }
        return throwError(error);
      })
    );
  }
}
