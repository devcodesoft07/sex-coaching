export interface IAboutUs {
  [id: string]: any
}


export interface IAboutUsProfile {
  id: string;
  avatar: string;
  description: string;
  specialties: string[],
  subtitle: string;
  title: string;
}

export interface IProfileSocialNetwork {
  id: string;
  icon: string;
  title: string;
  url: string;
}

export interface IAboutUsCallToAction {
  id: string;
  description: string;
  images: string[],
  subtitle: string;
  title: string;
}

export interface IAboutUsBanner {
  id: string;
  image: string,
  subtitle: string;
  title: string;
}
