import { Timestamp } from "@angular/fire/firestore";

export interface IBlog {
  id: string;
  title: string;
  subtitle: string;
  description: string;
  banner: string;
  category: ECategoryBlog,
  url: string;
  datePublish: Timestamp;
  folderName: string;
}

export interface IBlogPost extends Omit<IBlog, 'id'|'datePublish' > {
  datePublish: Date;
}

export enum ECategoryBlog {
  SHARED = 'shared',
  OWN = 'own'
}
