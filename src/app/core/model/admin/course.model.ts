import { Timestamp } from "@angular/fire/firestore";
export interface ICourse{
  id: string;
  title: string;
  description: string;
  category: string;
  price: number;
  banner: string;
  dateStart: Timestamp;
  timeDuration?: number;
  folderName: string;
}

