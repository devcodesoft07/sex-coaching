export interface ISpeciality {
  id: string;
  title: string;
  description: string;
  icon: string;
  folderName: string;
}
