import { Country } from "../countrie.model";

export interface ITestimonial {
  id: string;
  name: string;
  avatar: string;
  country: Country;
  testimonial: string;
}
