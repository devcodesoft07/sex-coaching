import { Timestamp } from "@angular/fire/firestore";
import { UserMetadata } from "firebase/auth";
import { Country } from "../countrie.model";
import { ICourse } from "./course.model";

export interface IUser {
  id: string;
  name: string;
  role: ERole;
  avatar: string;
  country: Country | string;
  email: string;
  password: string;
  dateStart: Timestamp | Date;
}

export enum ERole {
  SUBSCRIBER = 'SUBSCRIBER',
  ADMIN = 'ADMIN'
}

export interface IBuyedCourse {
  id: string;
  course: ICourse;
  dateStart: Timestamp;
  dateEnd: Timestamp;
  numberMonth: number;
  approved: boolean;
}

export interface IBuyedCoursePost {
  course: ICourse;
  dateStart: Date;
  dateEnd: Date;
  numberMonth: number;
  approved: boolean;
  voucher: string;
}
