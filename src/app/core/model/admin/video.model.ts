export interface IVideo {
  id?: string;
  title: string;
  src: string;
  type?: string;
  description?: string;
  order?: number;
}
