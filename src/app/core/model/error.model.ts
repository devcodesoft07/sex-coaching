import { HttpHeaders } from "@angular/common/http";

export interface IErrorsMessage {
	error?: IInternalError;
	status: number;
	message?: string;
  header?: HttpHeaders;
  name?: string;
  ok?: boolean;
  statusText?: string;
  url?: string;
}

interface IInternalError {
  status: boolean;
  message: string;
  errors: any;
}
