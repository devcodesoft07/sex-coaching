export interface IGlobalModel {
  id: number;
  name: string;
  created_at?: Date;
  update_at?: Date;
  status?: boolean;
}
