export interface ILinksCO {
  link: string;
  icon: string;
  label: string;
}


export interface ILinks {
  links_admin: ILinksCO[];
  links_order_external_plant: ILinksCO[];
  links_order_last_mille: ILinksCO[];
  links_warehouse: ILinksCO[];
}
