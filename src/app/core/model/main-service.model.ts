export interface IMainService {
  id: string;
  icon: string;
  title: string;
  description: string;
}
