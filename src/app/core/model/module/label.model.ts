import { IGlobalModel } from "../global.model";

export interface ILabelGet extends IGlobalModel {
  prefix: string;
  color: string;
  type: string;
}

export interface ILabelPost extends Omit<ILabelGet, 'id, created_at, update_at, status'> {

}
