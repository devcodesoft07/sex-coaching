export interface IAuthUser {
  email: string;
  password: string;
}

export interface IAuthResponse {
  access_token: string;
  token_type: string;
  expires_at: string;
}
