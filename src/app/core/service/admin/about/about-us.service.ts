import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { IAboutUs, IAboutUsBanner, IAboutUsCallToAction, IAboutUsProfile, IProfileSocialNetwork } from 'src/app/core/model/admin/about-us.model';
@Injectable({
  providedIn: 'root'
})
export class AboutUsService {

  constructor(
    private firestoreService: AngularFirestore
  ) { }

  public getAll(): Observable<IAboutUs[]> {
    return this.firestoreService.collection<IAboutUs>('about-us').valueChanges({idField: 'id'});
  }

  // getData(id: string): Observable<(IAboutUs & { id: string; }) | undefined> {
  //   return this.firestoreService.collection<IAboutUs>('about-us').doc(id).valueChanges({idField: 'id'});
  // }

  public add(data: IAboutUs): Promise<DocumentReference<IAboutUs>> {
    return this.firestoreService.collection<IAboutUs>('about-us').add(data);
  }

  public update(data: IAboutUs | any): Promise<void> {
    return this.firestoreService.collection<IAboutUs>('about-us').doc(data.id).update(data);
  }

  public delete(data: IAboutUs | any): Promise<void> {
    return this.firestoreService.collection<IAboutUs>('about-us').doc(data.id).delete();
  }

  public doGetBanner(): Observable<IAboutUsBanner> {
    return this.firestoreService.collection<any>('about-us').doc('banner').valueChanges({idField: 'id'});
  }

  public doGetCallToAction(): Observable<IAboutUsCallToAction> {
    return this.firestoreService.collection<any>('about-us').doc('call-to-action').valueChanges({idField: 'id'});
  }

  public doGetProfile(): Observable<IAboutUsProfile> {
    return this.firestoreService.collection<any>('about-us').doc('profile').valueChanges({idField: 'id'});
  }

  public doGetProfileSocialNetwork(): Observable<IProfileSocialNetwork[]> {
    return this.firestoreService.collection<IAboutUs>('about-us').doc('profile').collection<IProfileSocialNetwork>('social-network').valueChanges({idField: 'id'});
  }

}
