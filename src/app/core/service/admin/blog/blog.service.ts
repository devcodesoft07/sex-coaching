import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { IBlog, IBlogPost } from 'src/app/core/model/admin/blog.model';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getAll(): Observable<IBlog[]> {
    return this.firestoreService.collection<IBlog>('blogs', ref => ref.orderBy('datePublish', 'asc')).valueChanges({idField: 'id'});
  }

  getData(id: string): Observable<(IBlog & { id: string; }) | undefined> {
    return this.firestoreService.collection<IBlog>('blogs').doc(id).valueChanges({idField: 'id'});
  }

  add(data: IBlogPost): Promise<DocumentReference<IBlogPost | IBlog>> {
    return this.firestoreService.collection<IBlogPost | IBlog>('blogs').add(data);
  }

  update(data: IBlog | any): Promise<void> {
    return this.firestoreService.collection<IBlog>('blogs').doc(data.id).update(data);
  }

  delete(data: IBlog | any): Promise<void> {
    return this.firestoreService.collection<IBlog>('blogs').doc(data.id).delete();
  }
}
