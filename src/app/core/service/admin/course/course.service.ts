import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { IBuyedCourse, IUser } from 'src/app/core/model/admin/user.model';
import { IVideo } from 'src/app/core/model/admin/video.model';
@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getAll(): Observable<ICourse[]> {
    return this.firestoreService.collection<ICourse>('courses').valueChanges({idField: 'id'});
  }

  getAllByUser(user: IUser): Observable<ICourse[]> {
    return this.firestoreService.collection<IUser>('users').doc(user?.id as string).collection<IBuyedCourse>('courses').valueChanges({idField: 'id'})
    .pipe(
      map(
        (data: IBuyedCourse[]) => {
          const courses : ICourse[] = [];
          data.forEach(
            (data: IBuyedCourse) => {
              if (data.approved) {
                courses.push(data.course);
              }
            }
          );
          return courses;
        }
      )

    );
  }

  getData(id: string): Observable<(ICourse & { id: string; }) | undefined> {
    return this.firestoreService.collection<ICourse>('courses').doc(id).valueChanges({idField: 'id'});
  }

  add(data: ICourse): Promise<DocumentReference<ICourse>> {
    return this.firestoreService.collection<ICourse>('courses').add(data);
  }

  update(data: ICourse | any): Promise<void> {
    return this.firestoreService.collection<ICourse>('courses').doc(data.id).update(data);
  }

  delete(data: ICourse | any): Promise<void> {
    return this.firestoreService.collection<ICourse>('courses').doc(data.id).delete();
  }

  addVideo(data: IVideo, course: ICourse): Promise<DocumentReference<IVideo>> {
    return this.firestoreService.collection<ICourse>('courses').doc(course.id).collection<IVideo>('videos').add(data);
  }

  updateVideo(data: IVideo, course: ICourse): Promise<void> {
    return this.firestoreService.collection<ICourse>('courses').doc(course.id).collection<IVideo>('videos').doc(data.id).update(data);
  }

  deleteVideo(data: IVideo, course: ICourse | undefined): Promise<void> {
    return this.firestoreService.collection<ICourse>('courses').doc(course?.id).collection<IVideo>('videos').doc(data.id).delete();
  }

  getAllVideos(course: ICourse | undefined): Observable<IVideo[]> {
    return this.firestoreService.collection<ICourse>('courses').doc(course?.id).collection<IVideo>('videos', ref => ref.orderBy('order', 'asc')).valueChanges({idField: 'id'});
  }
}
