import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { ISpeciality } from 'src/app/core/model/admin/speciality.model';
@Injectable({
  providedIn: 'root'
})
export class SpecialityService {

  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getAll(): Observable<ISpeciality[]> {
    return this.firestoreService.collection<ISpeciality>('specialities', ref => ref.orderBy('title', 'asc')).valueChanges({idField: 'id'});
  }

  getData(id: string): Observable<(ISpeciality & { id: string; }) | undefined> {
    return this.firestoreService.collection<ISpeciality>('specialities').doc(id).valueChanges({idField: 'id'});
  }

  add(data: ISpeciality): Promise<DocumentReference<ISpeciality>> {
    return this.firestoreService.collection<ISpeciality>('specialities').add(data);
  }

  update(data: ISpeciality | any): Promise<void> {
    return this.firestoreService.collection<ISpeciality>('specialities').doc(data.id).update(data);
  }

  delete(data: ISpeciality | any): Promise<void> {
    return this.firestoreService.collection<ISpeciality>('specialities').doc(data.id).delete();
  }
}
