import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { ITestimonial } from 'src/app/core/model/admin/testimonial.model';
@Injectable({
  providedIn: 'root'
})
export class TestimonialService {

  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getAll(): Observable<ITestimonial[]> {
    return this.firestoreService.collection<ITestimonial>('testimonials', ref => ref.orderBy('name', 'asc')).valueChanges({idField: 'id'});
  }

  getData(id: string): Observable<(ITestimonial & { id: string; }) | undefined> {
    return this.firestoreService.collection<ITestimonial>('testimonials').doc(id).valueChanges({idField: 'id'});
  }

  add(data: ITestimonial): Promise<DocumentReference<ITestimonial>> {
    return this.firestoreService.collection<ITestimonial>('testimonials').add(data);
  }

  update(data: ITestimonial | any): Promise<void> {
    return this.firestoreService.collection<ITestimonial>('testimonials').doc(data.id).update(data);
  }

  delete(data: ITestimonial | any): Promise<void> {
    return this.firestoreService.collection<ITestimonial>('testimonials').doc(data.id).delete();
  }
}
