import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { IBuyedCourse, IBuyedCoursePost, IUser } from 'src/app/core/model/admin/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getAll(): Observable<IUser[]> {
    return this.firestoreService.collection<IUser>('users', ref => ref.orderBy('name', 'asc')).valueChanges({idField: 'id'});
  }

  getData(id: string): Observable<(IUser & { id: string; }) | undefined> {
    return this.firestoreService.collection<IUser>('users').doc(id).valueChanges({idField: 'id'});
  }

  add(data: IUser, uid?: string): Promise<void> {
    return this.firestoreService.collection<IUser>('users').doc(uid).set(data);
  }

  update(data: IUser | any): Promise<void> {
    return this.firestoreService.collection<IUser>('users').doc(data.id).update(data);
  }

  delete(data: IUser | any): Promise<void> {
    return this.firestoreService.collection<IUser>('users').doc(data.id).delete();
  }

  addCourses(data: IBuyedCourse | IBuyedCoursePost, user: IUser): Promise<DocumentReference<IBuyedCourse | IBuyedCoursePost>> {
    return this.firestoreService.collection<IUser>('users').doc(user?.id as string).collection<IBuyedCourse | IBuyedCoursePost>('courses').add(data);
  }

  getAllCourses(user: IUser): Observable<IBuyedCourse[]> {
    return this.firestoreService.collection<IUser>('users').doc(user?.id as string).collection<IBuyedCourse>('courses').valueChanges({idField: 'id'});
  }

  approvedCourseByUser(data: IBuyedCourse, user: IUser): Promise<void> {
    return this.firestoreService.collection<IUser>('users').doc(user?.id as string).collection<IBuyedCourse>('courses').doc(data.id).update(data);
  }

}
