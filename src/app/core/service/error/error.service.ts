import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }
  showMessageErrorClient(errorMessage: string, status: number): void {

    if (status === 0) {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Fallo en la conexión, verifica tu conexión a internet e intente nuevamente',
        showConfirmButton: true,
        customClass: 'swalConfirm'
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Error en el navegador del cliente',
        text: 'Verifica que tu navegador tenga habilidado lo neceseario para utilizar el sistema',
        showConfirmButton: true,
        customClass: 'swalConfirm'
      });
    }
  }
  showMessageErrorBackend(errorMessage: string, status: number): void {
    if (status === 401) {
      Swal.fire({
        icon: 'error',
        title: 'Usuario no autorizado',
        text: 'El correo electronico / contraseña son incorrectas',
        showConfirmButton: true,
        customClass: 'swalConfirm'
      });
    }
  }
}
