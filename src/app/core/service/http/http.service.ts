import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  customHeader;
  constructor(private http: HttpClient) {
    this.customHeader = new HttpHeaders();
    this.customHeader.append('Accept', 'application/json');
    this.customHeader.append('Content-Type', 'application/json' );
  }

  post(serviceName: string, data: any) {
    const options = { headers: this.customHeader, withCredintials: false };
    const url = environment.apiUrl + serviceName;
    return this.http.post(url, data, options);
  }

  get(serviceName: string) {
    const options = { headers: this.customHeader, withCredintials: false };
    const url = environment.apiUrl + serviceName;
    return this.http.get(url, options);
  }

  delete(serviceName: string) {
    const options = { headers: this.customHeader, withCredintials: false };
    const url = environment.apiUrl + serviceName;
    return this.http.delete(url, options);
  }

  put(serviceName: string, data: any) {
    const options = { headers: this.customHeader, withCredintials: false };
    const url = environment.apiUrl + serviceName + '/' + data.id;
    return this.http.put(url, data, options);
  }
}
