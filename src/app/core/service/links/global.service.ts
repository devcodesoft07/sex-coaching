import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthConstans } from '../../model/config/auth-constanst.config';
import { ILinks, ILinksCO } from '../../model/links-co.model';
import { IMainService } from '../../model/main-service.model';
import { ILabelGet } from '../../model/module/label.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  private _user: any;

  constructor(
    private httpService: HttpService,
    private httpClient: HttpClient,
    private cookieService: CookieService
  ) { }

  getLinks(): Observable<ILinks> {
    return this.httpClient.get<ILinks>("assets/json/links.json")
      .pipe(
        map(
          (res: any) => {
            if (this.cookieService.check(AuthConstans.USER)) {
              this._user = JSON.parse(this.cookieService.get(AuthConstans.USER));
            }
            if (this._user?.role === 'SUBSCRIBER') {
              const links = res.links_admin.filter((link: any) => link.link !== 'usuarios');
              res.links_admin = links;
              return res as ILinks
            }
            return res as ILinks
          }
        )
      );
  }

  getLabels(): Observable<ILabelGet[]> {
    return this.httpClient.get<ILabelGet[]>("assets/json/labels.json")
      .pipe(
        map(
          (res: any) => res as ILabelGet[]
        )
      );
  }
}
