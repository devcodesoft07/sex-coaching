import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IMainService } from '../../model/main-service.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class MainServiceService {

  constructor(
    private httpService: HttpService,
    private httpClient: HttpClient,
    private firestoreService: AngularFirestore
  ) { }

  getMainServiceList():Observable<IMainService[]> {
    return this.httpClient.get<IMainService[]>("assets/json/main-service.json")
    .pipe(
      map(
        (res: any) => res.main_service as IMainService[]
      )
    );
  }

  public generateRandomId(): string {
    let randomId: string = '';
    const characters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength: number = characters.length;
    for (let index = 0; index < 6; index++) {
      randomId += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return randomId;
  }

  public doGetServices(): Observable<IMainService[]> {
    return this.firestoreService.collection<IMainService>('services').valueChanges({idField: 'id'});
  }
}
