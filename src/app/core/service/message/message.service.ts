import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Swal, { SweetAlertIcon, SweetAlertOptions, SweetAlertPosition } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private _propertiesToShow!: SweetAlertOptions;

  constructor() { }

  public showMessageConfirmation(titleToShow: string = 'Title', messageToShow: string = 'Message', positionToShow: SweetAlertPosition, iconToShow: SweetAlertIcon, timerDuration: number = 1500): void {
    Swal.fire({
      position: positionToShow,
      icon: iconToShow,
      title: titleToShow,
      text: messageToShow,
      showConfirmButton: false,
      timer: timerDuration
    })
  }

  public showMessageErrorClient(errorMessage: string, status: number): void {
    if (status === 0) {
      this._propertiesToShow = {
        icon: 'error',
        title: 'Error',
        text: 'Fallo en la conexión, verifica tu conexión a internet e intente nuevamente',
        showConfirmButton: true,
      }
    } else {
      this._propertiesToShow = {
        icon: 'error',
        title: 'Error en el navegador del cliente',
        text: 'Verifica que tu navegador tenga habilidado lo neceseario para utilizar el sistema',
        showConfirmButton: true,
      }
    }
    this.showSwalBasicAlert(this._propertiesToShow);
  }

  public showMessageErrorBackend(errorMessage: string, status: number): void {
    this._propertiesToShow = {
      icon: 'warning',
      title: 'Advertencia',
      text: JSON.stringify(errorMessage),
      showConfirmButton: true,
    }
    if (status === 401) {
      this.showSwalBasicAlert(this._propertiesToShow);
    } if (status === 422) {
      this.showSwalBasicAlert(this._propertiesToShow);
    }
  }

  public showSwalConfirmAlert(propertiesToShowFromComponent: SweetAlertOptions): Observable<boolean> {
    this._propertiesToShow = propertiesToShowFromComponent;
    this._propertiesToShow.icon = 'question';
    this._propertiesToShow.showCancelButton = true;
    return new Observable(
      (observer) => {
        Swal.fire(this._propertiesToShow).then((result) => {
          if (result.isConfirmed) {
            observer.next(true);
          } else {
            Swal.close();
            observer.next(false);
          }
        })

      }
    );
  }

  public doGetIsConfirmed(): Observable<boolean> {
    return new Observable((observer) => {
      observer.next(true);
      observer.error('Has error');
    });
  }

  public showSwalBasicAlert(propertiesToShowFromComponent: SweetAlertOptions): void {
    Swal.fire(propertiesToShowFromComponent);
  }

}
