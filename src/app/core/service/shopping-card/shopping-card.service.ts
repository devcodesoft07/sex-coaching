import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ICourse } from '../../model/admin/course.model';
import { MessageService } from '../message/message.service';
import { SweetAlertIcon, SweetAlertPosition } from 'sweetalert2';
import { VariablesConstanst } from '../../config/variables.config';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  private courses: ICourse[];

  private shoppingCart$: BehaviorSubject<ICourse[]>;
  public coursList$: Observable<ICourse[]>;

  private title!: string;
  private message!: string;
  private position!: SweetAlertPosition;
  private icon!: SweetAlertIcon;

  constructor( private messageService: MessageService) {
    this.courses = [];
    this.shoppingCart$ = new BehaviorSubject<ICourse[]>([]);
    this.coursList$ = this.shoppingCart$.asObservable();
    this._initialize();
  }

  private _initialize(): void {
    this._getCourseListFromStorage();
  }

  private _getCourseListFromStorage(): void {
    const shoppingCartStorage = localStorage.getItem(VariablesConstanst.SHOPPING_CART);
    if (shoppingCartStorage) {
      const dataFromStorage: string | null = localStorage.getItem(VariablesConstanst.SHOPPING_CART);
      const courseListFromStorage: ICourse[] | null = JSON.parse(dataFromStorage? dataFromStorage : '[]') as ICourse[];
      this.courses = courseListFromStorage;
      this.shoppingCart$.next(this.courses);
    }
  }

  public addCourseToList(course: ICourse): void {
    if (!this.verifyIsExistCourseOnCart(course)) {
      this.courses = [...this.courses, course];
      this.shoppingCart$.next(this.courses);
      localStorage.setItem(VariablesConstanst.SHOPPING_CART, JSON.stringify(this.courses));
      this.title = '!Agregado!';
      this.message = `El curso ${course.title} se agregó al carrito`;
      this.position = 'top-end';
      this.icon = 'success';
      this.messageService.showMessageConfirmation(this.title, this.message, this.position, this.icon);
    } else {
      this.title = '!Advertencia!';
      this.message = `El curso ${course.title} ya existe en el carrito`;
      this.position = 'center';
      this.icon = 'warning';
      this.messageService.showMessageConfirmation(this.title, this.message, this.position, this.icon);
    }
  }

  public deleteCourseToList(course: ICourse): void {
    const indice = this.courses.indexOf(course);
    this.courses.splice(indice, 1);
    this.shoppingCart$.next(this.courses);
    this.title = '!Eliminado!';
    this.message = `El curso ${course.title} se eliminó de tu carrito`;
    this.position = 'center';
    this.icon = 'success';
    this.messageService.showMessageConfirmation(this.title, this.message, this.position, this.icon);

    localStorage.setItem(VariablesConstanst.SHOPPING_CART, JSON.stringify(this.courses));
  }

  public verifyIsExistCourseOnCart(courseToVerify: ICourse): boolean {
    const courseFoud: ICourse[] = this.courses.filter((course: ICourse) => course.id === courseToVerify.id);
    return courseFoud.length === 0 ? false : true;
  }

  public reset(): void {
    this.courses = [];
    this.shoppingCart$.next(this.courses);
  }
}
