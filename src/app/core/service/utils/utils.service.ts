import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  private _characters: string;
  constructor() {
    this._characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  }

  public makeId(): string {
    let result: string = '';
    const charactersLength: number = this._characters.length;
    const charactersAmountOfId: number = 8;
    for (var i = 0; i < charactersAmountOfId; i++) {
      result += this._characters.charAt(Math.floor(Math.random() *
        charactersLength));
    }
    return result;
  }

  public sleep(miliSecondsToSleep: number): Promise<void> {
    return new Promise((accept) => {
      setTimeout(() => {
        accept();
      }, miliSecondsToSleep);
    },);
  }
}
