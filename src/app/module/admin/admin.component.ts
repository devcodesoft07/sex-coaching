import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MatSidenav } from '@angular/material/sidenav';
import { MenuService } from 'src/app/core/service/menu/menu.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminComponent implements AfterViewInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
  );
  @ViewChild('sidenavAdmin') sideNav!: MatSidenav;
  constructor(
    private breakpointObserver: BreakpointObserver,
    private menuService: MenuService,
  ) {

  }
  ngAfterViewInit(): void {
    this.menuService.setSidenav(this.sideNav);
    // this.authService.authStateChange();
    // this._cdr.detectChanges();
  }

}
