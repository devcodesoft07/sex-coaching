import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Modules
import { AdminRoutingModule } from './admin-routing.module';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { SharedModule } from 'src/app/shared/shared.module';
//Components
import { AdminComponent } from './admin.component';


@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    AngularMaterialModule,
    SharedModule
  ]
})
export class AdminModule { }
