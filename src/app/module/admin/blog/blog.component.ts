import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { IBlog } from 'src/app/core/model/admin/blog.model';
import { BlogService } from 'src/app/core/service/admin/blog/blog.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AddBlogComponent } from './components/add-blog/add-blog.component';
import { CookieService } from 'ngx-cookie-service';
import { IUser } from 'src/app/core/model/admin/user.model';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';
import { AuthService } from 'src/app/core/service/auth/auth.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  blogs!: IBlog[];
  state: boolean = false;
  dataToSearch: string = '';
  user!: IUser;
  constructor(
    private blogService: BlogService,
    private breakpointObserver: BreakpointObserver,
    private dialog: MatDialog,
    private cookieService: CookieService,
    private authService: AuthService
  ) {
    this._initialize();
  }

  private _initialize() {
    if (this.cookieService.check(AuthConstans.USER)) {
      this.user = JSON.parse(this.cookieService.get(AuthConstans.USER));
    }
  }

  ngOnInit(): void {
    this.getAllData();
  }

  getAllData() {
    this.state = true;
    this.blogService.getAll()
    .subscribe(
      (res: IBlog[]) => {
        this.state = false;
        this.blogs = res;
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      }
    );
  }

  addNewData(): void {
    const dialogRef: MatDialogRef<AddBlogComponent> = this.dialog.open(AddBlogComponent, {
      width: '500px',
      height: '500px',
      panelClass: 'modal-form',
    });
  }

}
