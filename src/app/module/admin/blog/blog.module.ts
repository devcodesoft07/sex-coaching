import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//MOdules
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { BlogRoutingModule } from './blog-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//Components
import { BlogComponent } from './blog.component';
import { DataBlogContainer } from './container/data-blog/data-blog.container';
import { AddBlogComponent } from './components/add-blog/add-blog.component';
import { EditBlogComponent } from './components/edit-blog/edit-blog.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';


@NgModule({
  declarations: [
    BlogComponent,
    DataBlogContainer,
    AddBlogComponent,
    EditBlogComponent
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    SweetAlert2Module
  ]
})
export class BlogModule { }
