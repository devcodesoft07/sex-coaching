import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
//RXJS
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
//Services
import { BlogService } from 'src/app/core/service/admin/blog/blog.service';
//Firebase
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { DocumentReference } from '@angular/fire/compat/firestore';
//Models
import { ECategoryBlog, IBlog, IBlogPost } from 'src/app/core/model/admin/blog.model';

import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import Swal from 'sweetalert2';
import { UtilsService } from 'src/app/core/service/utils/utils.service';
@Component({
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.scss']
})
export class AddBlogComponent implements OnInit {
  dataForm!: FormGroup;
  stateUpload: boolean = false;
  uploadPercent!: Observable<number | undefined>;
  downloadURL!: Observable<string>;
  categories: ECategoryBlog[] = [ECategoryBlog.OWN, ECategoryBlog.SHARED];
  today: Date = new Date();
  get title() {
    return this.dataForm.get('title');
  };
  get subtitle() {
    return this.dataForm.get('description');
  };
  get category() {
    return this.dataForm.get('title');
  };

  public errorsMessages = {
    title: [
      { type: 'required', message: 'Titulo es requerido' },
      { type: 'pattern', message: 'Solo se permiten letras' },
    ],
    subtitle: [
      { type: 'required', message: 'Description es requerido' },
      { type: 'maxlength', message: 'Maximo 170 letras' },
    ],
    category: [
      { type: 'required', message: 'Description es requerido' },
      { type: 'maxlength', message: 'Maximo 170 letras' },
    ],
  };
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddBlogComponent>,
    private storage: AngularFireStorage,
    private blogService: BlogService,
    private utilService: UtilsService
  ) {
    this.buildForm();
  }

  buildForm(): void {
    this.dataForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      subtitle: ['', [Validators.required]],
      category: ['', [Validators.required]],
      banner: ['https://images.unsplash.com/photo-1646317136848-4e527abe6e6a?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170', [Validators.required]],
      url: [''],
      description: [''],
      folderName: [this.utilService.makeId()],
      date: [this.today]
    });
  }

  ngOnInit(): void {
  }

  closeBtn(): void {
    this.dialogRef.close();
  }

  addData(swal: SwalComponent): void {
    const dataFormForm = this.dataForm.value;
    const data: IBlogPost = {
      datePublish: dataFormForm.date,
      title: dataFormForm.title,
      subtitle: dataFormForm.subtitle,
      category: dataFormForm.category,
      banner: dataFormForm.banner,
      url: dataFormForm.url,
      description: dataFormForm.description,
      folderName: dataFormForm.folderName
    };
    Swal.fire({
      title: '¿Está seguro de agregar el nuevo blog?',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Agregar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Guardando...!',
          html: 'Espere un momento, los datos se estan registrando',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.blogService.add(data)
            .then(
              (res: DocumentReference<IBlog | IBlogPost>) => {
                swal.fire();
                this.closeBtn();
              }
            )
            .catch(
              (error: any) => {
                console.log(error);
                Swal.close();
              }
            )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }

  uploadFile(event: any) {
    const folderName: string = this.dataForm.value.folderName;
    this.stateUpload = true;
    const file = event.target.files[0];
    const filePath = `blog/${folderName}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(
            (res: string) => {
              this.dataForm.controls.banner.setValue(res);
            }
          );
        })
     )
    .subscribe()
  }

}
