import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataBlogContainer } from './data-blog.container';

describe('DataBlogComponent', () => {
  let component: DataBlogContainer;
  let fixture: ComponentFixture<DataBlogContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataBlogContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataBlogContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
