import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { IBlog } from 'src/app/core/model/admin/blog.model';
import { IUser } from 'src/app/core/model/admin/user.model';
import { BlogService } from 'src/app/core/service/admin/blog/blog.service';
import Swal from 'sweetalert2';
import { EditBlogComponent } from '../../components/edit-blog/edit-blog.component';

@Component({
  selector: 'int-ero-blog',
  templateUrl: './data-blog.container.html',
  styleUrls: ['./data-blog.container.scss']
})
export class DataBlogContainer implements OnInit {
  @Input() blog!: IBlog;
  @Input() user!: IUser;
  constructor(  private blogService: BlogService,
                private storage: AngularFireStorage,
                private dialogController: MatDialog ) { }

  ngOnInit(): void {
  }

  editData(): void {
    const dialogRef: MatDialogRef<EditBlogComponent, any> = this.dialogController.open(EditBlogComponent, {
      width: '600px',
      height: '600px',
      panelClass: 'modal-form',
      data: this.blog
    });
  }

  deleteData(swal: SwalComponent): void {
    Swal.fire({
      title: `¿Está seguro de eliminar el blog ${this.blog.title}?`,
      text: 'Una vez eliminado no se podrá recuperar los datos.',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Eliminando...!',
          html: 'Espere un momento, estamos eliminando el blog',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.blogService.delete(this.blog)
            .then(
              () => {
                swal.fire();
                if (this.blog.banner !== '' || this.blog.banner !== null && this.blog.banner.includes('firebase')) {
                  this.storage.storage.refFromURL(this.blog.banner).delete();
                }
              }
            )
            .catch(
              (error: any) => {
                console.log(error);
                Swal.close();
              }
            )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }
}
