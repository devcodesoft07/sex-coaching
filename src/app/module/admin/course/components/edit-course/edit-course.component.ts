import { Component, Inject, OnInit } from '@angular/core';
import { DocumentReference } from '@angular/fire/compat/firestore';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ConstanstEnum } from 'src/app/core/enum/constanst.enum';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { CourseService } from 'src/app/core/service/admin/course/course.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.scss']
})
export class EditCourseComponent implements OnInit {
  courseForm!: FormGroup;
  stateUpload: boolean = false;
  uploadPercent!: Observable<number | undefined>;
  downloadURL!: Observable<string>;

  get title() {
    return this.courseForm.get('title');
  };
  get price() {
    return this.courseForm.get('price');
  };
  get video() {
    return this.courseForm.get('video');
  };
  get description() {
    return this.courseForm.get('description');
  };
  public errorsMessages = {
    title: [
      { type: 'required', message: 'Titulo es requerido' },
      { type: 'pattern', message: 'Solo se permiten letras' },
      { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    description: [
      { type: 'required', message: 'Descripcion es requerido' },
      // { type: 'pattern', message: 'Solo se permiten letras' },
      // { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    price: [
      { type: 'required', message: 'Precio es requerido' },
      { type: 'pattern', message: 'Solo se permiten números' },
      // { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    video: [
      { type: 'required', message: 'Video es requerido' },
      { type: 'pattern', message: 'Solo se permiten letras' },
      { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
  };
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditCourseComponent>,
    private storage: AngularFireStorage,
    private courseService: CourseService,
    @Inject(MAT_DIALOG_DATA) public data: ICourse
  ) {
    this.buildForm();
   }
  buildForm(): void {
    this.courseForm = this.formBuilder.group({
      id: [''],
      title: ['',
        [
          Validators.required,
        ]
      ],
      description: ['',
        [
          Validators.required,
        ]
      ],
      banner: ['https://images.unsplash.com/photo-1592398276785-f636168c02e0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80',
        [
          Validators.required,
        ]
      ],
      dateStart: ['',
        [
          Validators.required,
        ]
      ],
      price: ['',
        [
          Validators.required,
        ]
      ],
      folderName: [''],
    });
  }

  ngOnInit(): void {
    this.courseForm.patchValue(this.data);
    this.courseForm.controls.dateStart.setValue(this.data.dateStart.toDate());
  }

  closeBtn(): void {
    this.dialogRef.close();
  }

  addData(swal: SwalComponent): void {
    const data: ICourse = this.courseForm.value;
    Swal.fire({
      title: '¿Está seguro de guardar los cambios?',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Guardar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Guardando...!',
          html: 'Espere un momento, los datos se estan registrando',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.courseService.update(data)
            .then(
              () => {
                swal.fire();
                this.closeBtn();
              }
            )
            .catch(
              (error: any) => {
                console.log(error);
                Swal.close();
              }
            )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }


  uploadFile(event: any, swal: SwalComponent) {
    swal.title = 'Subido correctamente';
    swal.text = 'La imagen se subió correctamete';
    const folderName: string = this.courseForm.value.folderName;
    this.stateUpload = true;
    const file = event.target.files[0];
    const filePath = `courses/${folderName}/banner-${folderName}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(
            (res: string) => {
              swal.fire();
              this.stateUpload = false;
              this.courseForm.controls.banner.setValue(res);
            }
          );
        })
     )
    .subscribe()
  }

}
