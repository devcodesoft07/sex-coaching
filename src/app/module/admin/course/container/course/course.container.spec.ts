import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseContainer } from './course.container';

describe('CourseComponent', () => {
  let component: CourseContainer;
  let fixture: ComponentFixture<CourseContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
