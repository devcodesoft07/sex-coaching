import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { IUser } from 'src/app/core/model/admin/user.model';
import { CourseService } from 'src/app/core/service/admin/course/course.service';
import Swal from 'sweetalert2';
import { EditCourseComponent } from '../../components/edit-course/edit-course.component';
// import { AddCourseComponent } from '../../components/add-course/add-course.component';

@Component({
  selector: 'app-course-container',
  templateUrl: './course.container.html',
  styleUrls: ['./course.container.scss']
})
export class CourseContainer implements OnInit {
  @Input() course!: ICourse;
  @Input() user!: IUser;
  constructor(
    private dialogController: MatDialog,
    private storage: AngularFireStorage,
    private courseService: CourseService
  ) { }

  ngOnInit(): void {
  }

  editData(): void {
    const dialogRef: MatDialogRef<EditCourseComponent, any> = this.dialogController.open(EditCourseComponent, {
      width: '600px',
      height: '600px',
      panelClass: 'modal-form',
      data: this.course
    });
  }


  deleteData(swal: SwalComponent): void {
    Swal.fire({
      title: `¿Está seguro de eliminar el curso ${this.course.title}?`,
      text: 'Una vez eliminado no se podrá recuperar los datos.',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Eliminando...!',
          html: 'Espere un momento, estamos eliminando el curso',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.courseService.delete(this.course)
            .then(
              () => {
                swal.fire();
                if (this.course.banner !== '' || this.course.banner !== null) {
                  this.storage.storage.refFromURL(this.course.banner).delete();
                }
              }
            )
            .catch(
              (error: any) => {
                console.log(error);
                Swal.close();
              }
            )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }
}
