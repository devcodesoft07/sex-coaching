import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseComponent } from './course.component';

const routes: Routes = [
  {
    path: '', component: CourseComponent,
    children: [
    ]
  },
  {
    path: 'detalle-curso/:id',
    loadChildren: () => import('./page/course-detail/course-detail.module').then(m => m.CourseDetailModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseRoutingModule { }
