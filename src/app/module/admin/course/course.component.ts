import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { ERole, IUser } from 'src/app/core/model/admin/user.model';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';
import { CourseService } from 'src/app/core/service/admin/course/course.service';
import { AddCourseComponent } from './components/add-course/add-course.component';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {
  state: boolean = false;
  courses!: ICourse[];
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
  );
  dataToSearch: string = '';
  filterToApplied: string = '';
  user!: IUser;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private dialogController: MatDialog,
    private courseService: CourseService,
    private cookieService: CookieService
  ) {
    this._initialize();
  }

  private _initialize() {
    if (this.cookieService.check(AuthConstans.USER)) {
      this.user = JSON.parse(this.cookieService.get(AuthConstans.USER));
    }
  }

  ngOnInit(): void {
    if (this.user.role === ERole.ADMIN) {
      this.getAllData();
    } else {
      this.getAllDataByUser();
    }
  }

  getAllDataByUser() {
    this.state = true;
    this.courseService.getAllByUser(this.user)
    .subscribe(
      (res: ICourse[]) => {
        this.state = false;
        this.courses = res;
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      }
    );
  }

  getAllData() {
    this.state = true;
    this.courseService.getAll()
    .subscribe(
      (res: ICourse[]) => {
        this.state = false;
        this.courses = res;
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      }
    );
  }

  addCourse(){
    const dialogRef: MatDialogRef<AddCourseComponent, any> = this.dialogController.open(AddCourseComponent,{
      width: '600px',
      height: '600px',
      panelClass: 'modal-form'
    });
  }

  appliedFilter(filter: string): void {
    this.filterToApplied = filter;
  }

}
