import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseRoutingModule } from './course-routing.module';
import { CourseComponent } from './course.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { CourseContainer } from './container/course/course.container';
import { AvatarModule } from 'ngx-avatar';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddCourseComponent } from './components/add-course/add-course.component';
import { EditCourseComponent } from './components/edit-course/edit-course.component';


@NgModule({
  declarations: [
    CourseComponent,
    CourseContainer,
    AddCourseComponent,
    EditCourseComponent
  ],
  imports: [
    CommonModule,
    CourseRoutingModule,
    SharedModule,
    AngularMaterialModule,
    AvatarModule,
    SweetAlert2Module,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CourseModule { }
