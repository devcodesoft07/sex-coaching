import { Component, Inject, OnInit } from '@angular/core';
import { FirebaseError } from '@angular/fire/app';
import { DocumentReference } from '@angular/fire/compat/firestore';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { IVideo } from 'src/app/core/model/admin/video.model';
import { CourseService } from 'src/app/core/service/admin/course/course.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-syllabus',
  templateUrl: './add-syllabus.component.html',
  styleUrls: ['./add-syllabus.component.scss']
})
export class AddSyllabusComponent implements OnInit {

  dataForm!: FormGroup;
  stateUpload: boolean = false;
  uploadPercent!: Observable<number | undefined>;
  downloadURL!: Observable<string>;
  file!: File;
  playlist!: IVideo[];

  get title() {
    return this.dataForm.get('title');
  };
  get src() {
    return this.dataForm.get('src');
  };
  get description() {
    return this.dataForm.get('description');
  };
  public errorsMessages = {
    title: [
      { type: 'required', message: 'Titulo es requerido' },
      { type: 'pattern', message: 'Solo se permiten letras' },
      { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    description: [
      { type: 'required', message: 'Descripcion es requerido' },
      // { type: 'pattern', message: 'Solo se permiten letras' },
      // { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    src: [
      { type: 'required', message: 'Video es requerido' },
      { type: 'pattern', message: 'Solo se permiten letras' },
      { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
  };
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddSyllabusComponent>,
    private storage: AngularFireStorage,
    private courseService: CourseService,
    @Inject(MAT_DIALOG_DATA) public data: ICourse,
  ) {
    this.buildForm();
  }

  buildForm(): void {
    this.dataForm = this.formBuilder.group({
      title: ['',
        [
          Validators.required,
        ]
      ],
      description: ['',
        [
          Validators.required,
        ]
      ],
      src: ['',
        [
          Validators.required,
        ]
      ],
      type: ['',
        [
          Validators.required,
        ]
      ],
    });
  }

  ngOnInit(): void {
    this.getAllVideoss();
  }

  closeBtn(): void {
    this.dialogRef.close();
  }

  addData(swal: SwalComponent): void {
    this.getAllVideoss();
    const dataFromForm = this.dataForm.value;
    const video: IVideo = {
      title: dataFromForm.title,
      src: dataFromForm.src,
      order: this.playlist.length+1,
      type: dataFromForm.type,
      description: dataFromForm.description
    };
    Swal.fire({
      title: '¿Está seguro de agregar el nuevo video?',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Agregar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Guardando...!',
          html: 'Espere un momento, los datos se estan registrando',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.courseService.addVideo(video, this.data)
              .then(
                (res: DocumentReference<IVideo>) => {
                  swal.fire();
                  this.closeBtn();
                }
              )
              .catch(
                (error: any) => {
                  console.log(error);
                  Swal.close();
                }
              )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }

  uploadFile(event: any, swal: SwalComponent) {
    swal.title = 'Subido correctamente';
    swal.text = 'El video se subió correctamete';
    this.stateUpload = true;
    const file = event.target.files[0];
    this.file = file;
    this.dataForm.controls.type.setValue(this.file.type);
    this.dataForm.controls.title.setValue(this.file.name.toUpperCase());
    const filePath = `courses/${this.data.title}/Video-${this.file.name}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL = fileRef.getDownloadURL();
        this.downloadURL.subscribe(
          (res: string) => {
            this.stateUpload = false;
            this.dataForm.controls.src.setValue(res);
            swal.fire();
          }
        );
      })
    )
      .subscribe()
  }

  getAllVideoss() {
    this.courseService.getAllVideos(this.data)
      .subscribe(
        (res: IVideo[]) => {
          this.playlist = res;
          console.log('Tamanio lista', res.length);
        },
        (error: FirebaseError) => {
          console.log(error);
        }
      );
  }

}
