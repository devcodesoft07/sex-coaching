import { Component, Inject, OnInit } from '@angular/core';
import { DocumentReference } from '@angular/fire/compat/firestore';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { IVideo } from 'src/app/core/model/admin/video.model';
import { CourseService } from 'src/app/core/service/admin/course/course.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'int-ero-edit-syllabus',
  templateUrl: './edit-syllabus.component.html',
  styleUrls: ['./edit-syllabus.component.scss']
})
export class EditSyllabusComponent implements OnInit {
  dataForm!: FormGroup;
  stateUpload: boolean = false;
  uploadPercent!: Observable<number | undefined>;
  downloadURL!: Observable<string>;
  file!: File;
  get title() {
    return this.dataForm.get('title');
  };
  get src() {
    return this.dataForm.get('src');
  };
  get description() {
    return this.dataForm.get('description');
  };
  public errorsMessages = {
    title: [
      { type: 'required', message: 'Titulo es requerido' },
      { type: 'pattern', message: 'Solo se permiten letras' },
      { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    description: [
      { type: 'required', message: 'Descripcion es requerido' },
      // { type: 'pattern', message: 'Solo se permiten letras' },
      // { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
  };
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditSyllabusComponent>,
    private storage: AngularFireStorage,
    private courseService: CourseService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.buildForm();
  }

  buildForm(): void {
    this.dataForm = this.formBuilder.group({
      title: ['',
        [
          Validators.required,
        ]
      ],
      description: ['',
        [
          Validators.required,
        ]
      ],
      src: ['',
        [
          Validators.required,
        ]
      ],
      type: ['',
        [
          Validators.required,
        ]
      ],
    });
  }

  ngOnInit(): void {
    this.dataForm.patchValue(this.data.video);
  }

  closeBtn(): void {
    this.dialogRef.close();
  }

  save(swal: SwalComponent): void {
    const dataFromForm = this.dataForm.value;
    const video: IVideo = {
      title: dataFromForm.title,
      src: dataFromForm.src,
      type: dataFromForm.type,
      description: dataFromForm.description,
      id: this.data.video.id
    };
    Swal.fire({
      title: '¿Está seguro de guardar los cambios?',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Guardar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Guardando...!',
          html: 'Espere un momento, los datos se estan registrando',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.courseService.updateVideo(video, this.data.course)
            .then(
              () => {
                swal.fire();
                this.closeBtn();
              }
            )
            .catch(
              (error: any) => {
                console.log(error);
                Swal.close();
              }
            )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }

}
