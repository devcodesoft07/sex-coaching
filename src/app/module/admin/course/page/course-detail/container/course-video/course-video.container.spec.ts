import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseVideoContainer } from './course-video.container';

describe('CourseVideoComponent', () => {
  let component: CourseVideoContainer;
  let fixture: ComponentFixture<CourseVideoContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseVideoContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseVideoContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
