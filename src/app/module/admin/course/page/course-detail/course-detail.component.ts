import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Component, OnInit } from '@angular/core';
import { FirebaseError } from '@angular/fire/app';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { IVideo } from 'src/app/core/model/admin/video.model';
import { CourseService } from 'src/app/core/service/admin/course/course.service';
import Swal from 'sweetalert2';
import { AddSyllabusComponent } from './components/add-syllabus/add-syllabus.component';
import { EditSyllabusComponent } from './components/edit-syllabus/edit-syllabus.component';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';
import { IUser } from 'src/app/core/model/admin/user.model';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.scss']
})
export class CourseDetailComponent implements OnInit {
  showFiller = false;
  courseId!: string;
  course: (ICourse & { id: string; }) | undefined;
  playlist!: IVideo[];
  videoIndex = 0;
  video!: IVideo;
  state: boolean = false;
  user!: IUser;
  constructor(private router: Router,
    private dialogController: MatDialog,
    private activateRoute: ActivatedRoute,
    private courseService: CourseService,
    private storage: AngularFireStorage,
    private cookieService: CookieService,) {

    this._initialize();
  }

  private _initialize() {
    this.activateRoute.params.subscribe(
      (res: Params) => {
        this.courseId = res.id;
      }
    ).unsubscribe();
    if (this.cookieService.check(AuthConstans.USER)) {
      this.user = JSON.parse(this.cookieService.get(AuthConstans.USER));
    }
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.state = true;
    this.courseService.getData(this.courseId).subscribe(
      (res: (ICourse & { id: string; }) | undefined) => {
        this.course = res;
        this.getAllVideos();
      },
      (error: FirebaseError) => {
        this.state = false;
        console.log(error);
      }
    );
  }

  getAllVideos() {
    this.state = true;
    this.courseService.getAllVideos(this.course)
      .subscribe(
        (res: IVideo[]) => {
          this.state = false;
          this.playlist = res;
          // console.log('Videos', this.playlist.length);
          this.video = res[this.videoIndex];
        },
        (error: FirebaseError) => {
          this.state = false;
          console.log(error);
        }
      );
  }

  addVideo(course: any) {

    // const dataCourseDetail = {
    //   course: this.course,
    //   playListLength: this.playlist.length
    // }

    const dialogRef: MatDialogRef<AddSyllabusComponent, any> = this.dialogController.open(AddSyllabusComponent, {
      width: '600px',
      height: '600px',
      panelClass: 'modal-form',
      data: this.course
    });
  }

  editVideo(videoData: IVideo) {
    const dialogRef: MatDialogRef<EditSyllabusComponent, any> = this.dialogController.open(EditSyllabusComponent, {
      width: '600px',
      height: '600px',
      panelClass: 'modal-form',
      data: {
        video: videoData,
        course: this.course
      }
    });
  }


  deleteData(video: IVideo, swal: SwalComponent | undefined): void {
    Swal.fire({
      title: `¿Está seguro de eliminar el video ${video.title}?`,
      text: 'Una vez eliminado no se podrá recuperar los datos.',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Eliminando...!',
          html: 'Espere un momento, estamos eliminando el video',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.courseService.deleteVideo(video, this.course)
              .then(
                () => {
                  swal?.fire();
                  this.storage.storage.refFromURL(video.src).delete();
                }
              )
              .catch(
                (error: any) => {
                  console.log(error);
                  Swal.close();
                }
              )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }

  actionEvent(option: string, video: IVideo, swal?: SwalComponent): void {
    switch (option) {
      case 'edit':
        this.editVideo(video);
        break;
      case 'delete':
        this.deleteData(video, swal);
        break;

      default:
        break;
    }
  }

  changeVideo(data: any): void {
    this.onClickPlaylistItem(data.video, data.index);
  }

  onClickPlaylistItem(item: any, index: number) {
    this.videoIndex = index;
    this.video = item;
  }

  back(): void {
    this.router.navigate(['/secure/cursos']);
  }

}
