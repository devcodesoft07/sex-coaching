import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { CourseDetailRoutingModule } from './course-detail-routing.module';
import { CourseDetailComponent } from './course-detail.component';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { AvatarModule } from 'ngx-avatar';
import { SharedModule } from 'src/app/shared/shared.module';
import { VgCoreModule } from '@videogular/ngx-videogular/core';
import { VgControlsModule } from '@videogular/ngx-videogular/controls';
import { VgOverlayPlayModule } from '@videogular/ngx-videogular/overlay-play';
import { VgBufferingModule } from '@videogular/ngx-videogular/buffering';
import { AddSyllabusComponent } from './components/add-syllabus/add-syllabus.component';
import { CourseVideoContainer } from './container/course-video/course-video.container';
import { CourseDataListContainer } from './container/course-data-list/course-data-list.container';
import { ReactiveFormsModule } from '@angular/forms';
import { EditSyllabusComponent } from './components/edit-syllabus/edit-syllabus.component';


@NgModule({
  declarations: [
    CourseDetailComponent,
    AddSyllabusComponent,
    CourseVideoContainer,
    CourseDataListContainer,
    EditSyllabusComponent,
  ],
  imports: [
    CommonModule,
    CourseDetailRoutingModule,
    AngularMaterialModule,
    AvatarModule,
    SharedModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    SweetAlert2Module,
    ReactiveFormsModule
  ]
})
export class CourseDetailModule { }
