import { Component, OnChanges, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatStep } from '@angular/material/stepper';
import { CookieService } from 'ngx-cookie-service';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { IBuyedCoursePost, IUser } from 'src/app/core/model/admin/user.model';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';
import { ShoppingCartService } from 'src/app/core/service/shopping-card/shopping-card.service';
import { ShowFileComponent } from 'src/app/shared/components/qr-code/qr-code.component';
import * as moment from 'moment';
import { UserService } from 'src/app/core/service/admin/user/user.service';
import { MessageService } from 'src/app/core/service/message/message.service';
import { SweetAlertOptions } from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit, OnDestroy, OnChanges {
  public title: string = 'Selecciona el método de pago';
  @ViewChild('firstStepper') public firstStep!: MatStep;
  public user!: IUser;
  public imageUrlUpload: string;
  public loadingProgress: boolean;
  private _subs: Subscription;
  constructor(private matDialog: MatDialog,
    private _cookieService: CookieService,
    private _shoppingCartService: ShoppingCartService,
    private _userService: UserService,
    private _messageService: MessageService,
    private _router: Router,) {
    this.imageUrlUpload = '';
    this._subs = new Subscription();
    this.loadingProgress = false;
  }

  ngOnInit(): void {
    this._initialize();
  }

  ngOnChanges(): void {
    this._initialize();
  }


  ngOnDestroy(): void {
    this._unsubscriber();
  }

  private _unsubscriber(): void {
    this._subs.unsubscribe();
  }

  private _initialize(): void {
    this._getuser();
  }

  private _getuser(): void {
    setTimeout(() => {
      const ifExistUser: boolean = this._cookieService.check(AuthConstans.USER);
      if (ifExistUser) {
        this.user = JSON.parse(this._cookieService.get(AuthConstans.USER)) as IUser;
      }
    }, 1000);
  }

  public goToNextStepper(): void {
    this.firstStep._stepper.next();
  }

  public getUrlImage(imageUrlUpload: string): void {
    this.imageUrlUpload = imageUrlUpload;
    console.log(this.imageUrlUpload);
  }

  public showQrCode(image: string): void {
    const dialog: MatDialogRef<any> = this.matDialog.open(ShowFileComponent, {
      width: '500px',
      height: '500px',
      panelClass: 'modal-form',
      data: image
    });
  }

  public buyMyCourses(): void {
    this.loadingProgress = true;
    const swalProperties: SweetAlertOptions = {
      title: '¿Estas seguro de comprar los cursos seleccionados?',
      text: 'Verifica tu información, si todo esta correcto, prosige a terminar la operación',
      icon: 'question',
      showCloseButton: true,
      showConfirmButton: true,
      confirmButtonText: 'Si, comprar',
      cancelButtonText: 'Verificar datos',
    };

    const confirmAlert$ = this._messageService.showSwalConfirmAlert(swalProperties).pipe(
      map(
        (res: boolean) => {
          if (res) {
            const shoppingcart$: Subscription = this._shoppingCartService.coursList$.pipe(
              map(
                (courses: ICourse[]) => {
                  this._beginBuy(courses);
                }
              )
            ).subscribe();
            this._subs.add(shoppingcart$);
          }
        }
      )
    ).subscribe();
    this._subs.add(confirmAlert$);
  }

  private _beginBuy(courses: ICourse[]) {
    const swalProperties: SweetAlertOptions = {
      title: 'Cursos comprados exitosamente',
      text: 'Los datos se enviaron correctamente, se enviará la confirmación y aprobación una vez verificado la información para acceder a los cursos en un lapso de 48 horas.',
      showConfirmButton: true,
      icon: 'success'
    }

    const buyedCourses: IBuyedCoursePost[] = courses.map(
      (course: ICourse) => {
        return this._newBuyedCourse(course)
      }
    );

    let responses: boolean[] = [];

    buyedCourses.forEach(
      (buyedCourse: IBuyedCoursePost) => {
        this._userService.addCourses(buyedCourse, this.user).then(
          (res: any) => {
            responses.push(true);
            this._verifyIsSuccessAll(responses, buyedCourses, swalProperties);
          }
        );
      }
    );
  }

  private _verifyIsSuccessAll(responses: boolean[], buyedCourses: IBuyedCoursePost[], swalProperties: SweetAlertOptions) {
    if (responses.length === buyedCourses.length) {
      this._messageService.showSwalBasicAlert(swalProperties);
      this.loadingProgress = false;
      this._shoppingCartService.reset();
      this._router.navigateByUrl('/secure/cursos');
    }
  }

  private _newBuyedCourse(course: ICourse): IBuyedCoursePost {
    const currentDate = new Date();
    const futureMonth = moment(currentDate).add(1, 'M').toDate();
    const newBuyedCourse: IBuyedCoursePost = {
      course: course,
      dateStart: currentDate,
      dateEnd: futureMonth,
      numberMonth: 1,
      approved: false,
      voucher: this.imageUrlUpload
    };
    return newBuyedCourse;
  }
}
