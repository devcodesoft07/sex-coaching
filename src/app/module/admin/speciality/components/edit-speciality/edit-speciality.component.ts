import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
//RXJS
import { Observable } from 'rxjs';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
//Services
import { SpecialityService } from 'src/app/core/service/admin/speciality/speciality.service';
//Firebase
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { DocumentReference } from '@angular/fire/compat/firestore';
//Models
import { ISpeciality } from 'src/app/core/model/admin/speciality.model';

import Swal from 'sweetalert2';
import { finalize } from 'rxjs/operators';

@Component({
  templateUrl: './edit-speciality.component.html',
  styleUrls: ['./edit-speciality.component.scss']
})
export class EditSpecialityComponent implements OnInit {
  dataForm!: FormGroup;
  stateUpload: boolean = false;
  uploadPercent!: Observable<number | undefined>;
  downloadURL!: Observable<string>;


  get title() {
    return this.dataForm.get('title');
  };
  get description() {
    return this.dataForm.get('description');
  };
  public errorsMessages = {
    title: [
      { type: 'required', message: 'Titulo es requerido' },
      { type: 'pattern', message: 'Solo se permiten letras' },
    ],
    description: [
      { type: 'required', message: 'Description es requerido' },
      { type: 'maxlength', message: 'Maximo 170 letras' },
    ],
  };
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditSpecialityComponent>,
    private storage: AngularFireStorage,
    private specialityService: SpecialityService,
    @Inject(MAT_DIALOG_DATA) public data: ISpeciality
  ) {
    this.buildForm();
  }

  buildForm(): void {
    this.dataForm = this.formBuilder.group({
      id: [''],
      title: ['', [Validators.required]],
      description: [''],
      icon: [],
      folderName: []
    });
  }

  ngOnInit(): void {
    this.dataForm.patchValue(this.data);
  }

  closeBtn(): void {
    this.dialogRef.close();
  }

  addData(swal: SwalComponent): void {
    const data: ISpeciality = this.dataForm.value;
    Swal.fire({
      title: '¿Está seguro de guardar los cambios',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Guardar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Guardando...!',
          html: 'Espere un momento, los datos se estan guardando',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.specialityService.update(data)
            .then(
              () => {
                swal.fire();
                this.closeBtn();
              }
            )
            .catch(
              (error: any) => {
                console.log(error);
                Swal.close();
              }
            )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }

  uploadFile(event: any) {
    const folderName: string = this.dataForm.value.folderName;
    this.stateUpload = true;
    const file = event.target.files[0];
    const filePath = `specialities/${folderName}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(
            (res: string) => {
              this.dataForm.controls.icon.setValue(res);
            }
          );
        })
     )
    .subscribe()
  }

}
