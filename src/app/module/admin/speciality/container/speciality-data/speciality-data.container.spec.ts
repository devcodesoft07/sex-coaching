import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialityDataContainer } from './speciality-data.container';

describe('SpecialityDataComponent', () => {
  let component: SpecialityDataContainer;
  let fixture: ComponentFixture<SpecialityDataContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecialityDataContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialityDataContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
