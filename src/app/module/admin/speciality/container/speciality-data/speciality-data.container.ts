import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { ISpeciality } from 'src/app/core/model/admin/speciality.model';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import Swal from 'sweetalert2';
import { SpecialityService } from 'src/app/core/service/admin/speciality/speciality.service';
import { EditSpecialityComponent } from '../../components/edit-speciality/edit-speciality.component';
import { IUser } from 'src/app/core/model/admin/user.model';
@Component({
  selector: 'int-ero-speciality-data',
  templateUrl: './speciality-data.container.html',
  styleUrls: ['./speciality-data.container.scss']
})
export class SpecialityDataContainer implements OnInit {
  @Input() speciality!: ISpeciality;
  @Input() user!: IUser;
  constructor(
    private specialityService: SpecialityService,
    private dialogController: MatDialog,
    private storage: AngularFireStorage
  ) { }

  ngOnInit(): void {
  }


  editData(): void {
    const dialogRef: MatDialogRef<EditSpecialityComponent, any> = this.dialogController.open(EditSpecialityComponent, {
      width: '600px',
      height: '600px',
      panelClass: 'modal-form',
      data: this.speciality
    });
  }

  deleteData(swal: SwalComponent): void {
    Swal.fire({
      title: `¿Está seguro de eliminar la especialidad ${this.speciality.title}?`,
      text: 'Una vez eliminado no se podrá recuperar los datos.',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Eliminando...!',
          html: 'Espere un momento, estamos eliminando la especialidad',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.specialityService.delete(this.speciality)
            .then(
              () => {
                swal.fire();
                if (this.speciality.icon !== '' || this.speciality.icon !== null) {
                  this.storage.storage.refFromURL(this.speciality.icon).delete();
                }
              }
            )
            .catch(
              (error: any) => {
                console.log(error);
                Swal.close();
              }
            )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }

}
