import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { SpecialityService } from 'src/app/core/service/admin/speciality/speciality.service';
import { ISpeciality } from 'src/app/core/model/admin/speciality.model';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AddSpecialityComponent } from './components/add-speciality/add-speciality.component';
import { IUser } from 'src/app/core/model/admin/user.model';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';
@Component({
  templateUrl: './speciality.component.html',
  styleUrls: ['./speciality.component.scss']
})
export class SpecialityComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  specialities!: ISpeciality[];
  state: boolean = false;
  dataToSearch: string = '';
  user!: IUser;
  constructor(
    private specialityService: SpecialityService,
    private breakpointObserver: BreakpointObserver,
    private dialogController: MatDialog,
    private cookieService: CookieService
  ) {
    if (this.cookieService.check(AuthConstans.USER)) {
      this.user = JSON.parse(this.cookieService.get(AuthConstans.USER));
    }
  }

  ngOnInit(): void {
    this.getAllData();
  }

  getAllData() {
    this.state = true;
    this.specialityService.getAll()
    .subscribe(
      (res: ISpeciality[]) => {
        this.state = false;
        this.specialities = res;
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      }
    );
  }

  addNewData(): void {
    const dialogRef: MatDialogRef<AddSpecialityComponent, any> = this.dialogController.open(AddSpecialityComponent, {
      width: '600px',
      height: '600px',
      panelClass: 'modal-form',
    });
  }
}
