import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Modules
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpecialityRoutingModule } from './speciality-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
//Components
import { SpecialityComponent } from './speciality.component';
import { SpecialityDataContainer } from './container/speciality-data/speciality-data.container';
import { AddSpecialityComponent } from './components/add-speciality/add-speciality.component';
import { EditSpecialityComponent } from './components/edit-speciality/edit-speciality.component';
import { AvatarModule } from 'ngx-avatar';


@NgModule({
  declarations: [
    SpecialityComponent,
    SpecialityDataContainer,
    AddSpecialityComponent,
    EditSpecialityComponent
  ],
  imports: [
    CommonModule,
    SpecialityRoutingModule,
    SharedModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SweetAlert2Module,
    AvatarModule
  ]
})
export class SpecialityModule { }
