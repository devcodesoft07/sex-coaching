import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
//RXJS
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
//Services
import { TestimonialService } from 'src/app/core/service/admin/testimonial/testimonial.service';
import { CountryService } from 'src/app/core/service/country/country.service';
//Firebase
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { DocumentReference } from '@angular/fire/compat/firestore';
//Models
import { Country } from 'src/app/core/model/countrie.model';
import { ITestimonial } from 'src/app/core/model/admin/testimonial.model';
import { ConstanstEnum } from 'src/app/core/enum/constanst.enum';

import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import Swal from 'sweetalert2';
@Component({
  templateUrl: './add-testimonial.component.html',
  styleUrls: ['./add-testimonial.component.scss']
})
export class AddTestimonialComponent implements OnInit {
  dataForm!: FormGroup;
  stateUpload: boolean = false;
  uploadPercent!: Observable<number | undefined>;
  downloadURL!: Observable<string>;
  countries!: Country[]
  get name() {
    return this.dataForm.get('name');
  };
  get testimonial() {
    return this.dataForm.get('testimonial');
  };
  public errorsMessages = {
    name: [
      { type: 'required', message: 'Nombre es requerido' },
      { type: 'pattern', message: 'Solo se permiten letras' },
    ],
    testimonial: [
      { type: 'required', message: 'TEstimonio es requerido' },
      { type: 'maxlength', message: 'Maximo 170 letras' },
    ],
  };
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddTestimonialComponent>,
    private storage: AngularFireStorage,
    private testimonialService: TestimonialService,
    private countryService: CountryService
  ) {
    this.buildForm();
  }

  buildForm(): void {
    this.dataForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern(ConstanstEnum.PATTERN_ONLY_TEXT)]],
      testimonial: ['', [Validators.required, Validators.maxLength(170)]],
      country: ['', [Validators.required]],
      avatar: ['']
    });
  }

  ngOnInit(): void {
    this.getCountries();
  }

  getCountries() {
    this.countryService.getCountries().subscribe(
      (res: Country[]) => {
        this.countries = res;
      }
    );
  }

  closeBtn(): void {
    this.dialogRef.close();
  }

  addData(swal: SwalComponent): void {
    const data: ITestimonial = this.dataForm.value;
    Swal.fire({
      title: '¿Está seguro de agregar el nuevo testimonio?',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Agregar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Guardando...!',
          html: 'Espere un momento, los datos se estan registrando',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.testimonialService.add(data)
            .then(
              (res: DocumentReference<ITestimonial>) => {
                swal.fire();
                this.closeBtn();
              }
            )
            .catch(
              (error: any) => {
                console.log(error);
                Swal.close();
              }
            )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }

  uploadFile(event: any) {
    const name: string = this.dataForm.controls.name.value;
    this.stateUpload = true;
    const file = event.target.files[0];
    const filePath = `testimonial/user/${name}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(
            (res: string) => {
              this.dataForm.controls.avatar.setValue(res);
            }
          );
        })
     )
    .subscribe()
  }

}
