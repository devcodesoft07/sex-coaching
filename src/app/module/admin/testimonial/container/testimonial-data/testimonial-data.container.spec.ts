import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestimonialDataContainer } from './testimonial-data.container';

describe('TestimonialDataComponent', () => {
  let component: TestimonialDataContainer;
  let fixture: ComponentFixture<TestimonialDataContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestimonialDataContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestimonialDataContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
