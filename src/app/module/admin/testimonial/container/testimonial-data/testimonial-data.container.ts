import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { ITestimonial } from 'src/app/core/model/admin/testimonial.model';
import { IUser } from 'src/app/core/model/admin/user.model';
import { TestimonialService } from 'src/app/core/service/admin/testimonial/testimonial.service';
import Swal from 'sweetalert2';
import { EditTestimonialComponent } from '../../components/edit-testimonial/edit-testimonial.component';

@Component({
  selector: 'int-ero-testimonial',
  templateUrl: './testimonial-data.container.html',
  styleUrls: ['./testimonial-data.container.scss']
})
export class TestimonialDataContainer implements OnInit {
  @Input() testimonial!: ITestimonial;
  @Input() user!: IUser | undefined;
  constructor(
    private testimonialService: TestimonialService,
    private dialogController: MatDialog,
    private storage: AngularFireStorage
  ) { }

  ngOnInit(): void {
  }


    editData(): void {
    const dialogRef: MatDialogRef<EditTestimonialComponent, any> = this.dialogController.open(EditTestimonialComponent, {
      width: '600px',
      height: '600px',
      panelClass: 'modal-form',
      data: this.testimonial
    });
  }

  deleteData(swal: SwalComponent): void {
    Swal.fire({
      title: `¿Está seguro de eliminar el testimonio de ${this.testimonial.name}?`,
      text: 'Una vez eliminado no se podrá recuperar los datos.',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Eliminando...!',
          html: 'Espere un momento, estamos eliminando el testimonio',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.testimonialService.delete(this.testimonial)
            .then(
              () => {
                swal.fire();
                if (this.testimonial.avatar !== '' || this.testimonial.avatar !== null) {
                  this.storage.storage.refFromURL(this.testimonial.avatar).delete();
                }
              }
            )
            .catch(
              (error: any) => {
                console.log(error);
                Swal.close();
              }
            )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }
}
