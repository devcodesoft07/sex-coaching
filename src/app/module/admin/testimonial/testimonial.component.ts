import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ITestimonial } from 'src/app/core/model/admin/testimonial.model';
import { TestimonialService } from 'src/app/core/service/admin/testimonial/testimonial.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AddTestimonialComponent } from './components/add-testimonial/add-testimonial.component';
import { IUser } from 'src/app/core/model/admin/user.model';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';
@Component({
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.scss']
})
export class TestimonialComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  testimonials!: ITestimonial[];
  state: boolean = false;
  dataToSearch: string = '';
  user!: IUser | undefined;
  constructor(
    private testimonialService: TestimonialService,
    private breakpointObserver: BreakpointObserver,
    private dialogController: MatDialog,
    private cookieService: CookieService
  ) {
    this._initialize();
  }

  private _initialize() {
    if (this.cookieService.check(AuthConstans.USER)) {
      this.user = JSON.parse(this.cookieService.get(AuthConstans.USER));
    }
  }

  ngOnInit(): void {
    this.getAllData();
  }

  getAllData() {
    this.state = true;
    this.testimonialService.getAll()
    .subscribe(
      (res: ITestimonial[]) => {
        this.state = false;
        this.testimonials = res;
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      }
    );
  }

  addNewData(): void {
    const dialogRef: MatDialogRef<AddTestimonialComponent, any> = this.dialogController.open(AddTestimonialComponent, {
      width: '600px',
      height: '600px',
      panelClass: 'modal-form',
    });
  }

}
