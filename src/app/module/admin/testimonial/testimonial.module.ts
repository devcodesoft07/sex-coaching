import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Modules
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { TestimonialRoutingModule } from './testimonial-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AvatarModule } from 'ngx-avatar';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
//Components
import { TestimonialComponent } from './testimonial.component';
import { TestimonialDataContainer } from './container/testimonial-data/testimonial-data.container';
import { AddTestimonialComponent } from './components/add-testimonial/add-testimonial.component';
import { EditTestimonialComponent } from './components/edit-testimonial/edit-testimonial.component';

@NgModule({
  declarations: [
    TestimonialComponent,
    TestimonialDataContainer,
    AddTestimonialComponent,
    EditTestimonialComponent,
  ],
  imports: [
    CommonModule,
    TestimonialRoutingModule,
    AngularMaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    AvatarModule,
    SweetAlert2Module
  ]
})
export class TestimonialModule { }
