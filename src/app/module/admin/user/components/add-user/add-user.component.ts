import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
//RXJS
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
//Services
import { TestimonialService } from 'src/app/core/service/admin/testimonial/testimonial.service';
import { CountryService } from 'src/app/core/service/country/country.service';
//Firebase
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { DocumentReference } from '@angular/fire/compat/firestore';
//Models
import { Country } from 'src/app/core/model/countrie.model';
import { ITestimonial } from 'src/app/core/model/admin/testimonial.model';
import { ConstanstEnum } from 'src/app/core/enum/constanst.enum';

import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import Swal from 'sweetalert2';
import { ERole, IUser } from 'src/app/core/model/admin/user.model';
import { UserService } from 'src/app/core/service/admin/user/user.service';
import { IAuthUser } from 'src/app/core/model/user.model';
import { AuthService } from 'src/app/core/service/auth/auth.service';
import firebase from 'firebase/compat/app';

@Component({
  selector: 'int-ero-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  dataForm!: FormGroup;
  stateUpload: boolean = false;
  uploadPercent!: Observable<number | undefined>;
  downloadURL!: Observable<string>;
  countries!: Country[];
  date: Date;
  get name() {
    return this.dataForm.get('name');
  };
  get email() {
    return this.dataForm.get('email');
  };
  get phone() {
    return this.dataForm.get('phone');
  };
  get password() {
    return this.dataForm.get('password');
  };
  get country() {
    return this.dataForm.get('country');
  };
  public errorsMessages = {
    name: [
      { type: 'required', message: 'Nombre es requerido' },
      { type: 'pattern', message: 'Solo se permiten letras' },
    ],
    email: [
      { type: 'required', message: 'Email es requerido' },
      { type: 'email', message: 'example@example.com' },
    ],
    password: [
      { type: 'required', message: 'Password es requerido' },
      { type: 'minlenght', message: '6 caracteres como mínimo' },
    ],
    country: [
      { type: 'required', message: 'Pais es requerido' },
    ],
    phone: [
      { type: 'required', message: 'Número de celular es requerido' },
      { type: 'pattern', message: 'Solo se permite números' },
    ],
  };
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddUserComponent>,
    private storage: AngularFireStorage,
    private userService: UserService,
    private countryService: CountryService,
    private authService: AuthService
  ) {
    this.date = new Date();
    this.buildForm();
  }

  buildForm(): void {
    this.dataForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern(ConstanstEnum.PATTERN_ONLY_TEXT)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      country: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.pattern(ConstanstEnum.PATTERN_PHONE)]],
      role: [ERole.SUBSCRIBER],
      dateStart: [this.date],
      avatar: ['']
    });
  }

  ngOnInit(): void {
    this.getCountries();
  }

  getCountries() {
    this.countryService.getCountries().subscribe(
      (res: Country[]) => {
        this.countries = res;
      }
    );
  }

  closeBtn(): void {
    this.dialogRef.close();
  }

  addData(swal: SwalComponent): void {
    const data: IUser = this.dataForm.value;
    Swal.fire({
      title: '¿Está seguro de agregar al nuevo usuario?',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Agregar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Guardando...!',
          html: 'Espere un momento, los datos se estan registrando',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading();
            const authuser: IAuthUser = {
              email: data.email as string,
              password: data.password as string
            }
            this.authService.registerFire(authuser)
            .then(
              (res: firebase.auth.UserCredential) => {
                this.userService.add(data, res.user?.uid)
                .then(
                  () => {
                    swal.fire();
                    this.closeBtn();
                  }
                )
                .catch(
                  (error: any) => {
                    console.log(error);
                    Swal.close();
                  }
                )
              }
            );
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }

  uploadFile(event: any) {
    const name: string = this.dataForm.controls.name.value;
    this.stateUpload = true;
    const file = event.target.files[0];
    const filePath = `testimonial/user/${name}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(
            (res: string) => {
              this.dataForm.controls.avatar.setValue(res);
            }
          );
        })
     )
    .subscribe()
  }
}
