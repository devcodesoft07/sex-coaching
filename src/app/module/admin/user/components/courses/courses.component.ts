import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
//RXJS
import { Observable } from 'rxjs';
//Firebase
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { IBuyedCourse, IBuyedCoursePost, IUser } from 'src/app/core/model/admin/user.model';
import { UserService } from 'src/app/core/service/admin/user/user.service';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { CourseService } from 'src/app/core/service/admin/course/course.service';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ShowFileComponent } from 'src/app/shared/components/qr-code/qr-code.component';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'int-ero-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  showForm: boolean = false;
  dataForm!: FormGroup;
  stateUpload: boolean = false;
  uploadPercent!: Observable<number | undefined>;
  downloadURL!: Observable<string>;
  courses!: ICourse[];
  date: Date;
  coursesByUser: IBuyedCourse[] = [];
  displayedColumns: string[] = ['course', 'month', 'dateStart', 'dateEnd', 'check', 'action'];
  get numberMonth() {
    return this.dataForm.get('numberMonth');
  };
  get dateStart() {
    return this.dataForm.get('dateStart');
  };

  public errorsMessages = {
    numberMonth: [
      { type: 'required', message: 'Nombre es requerido' },
      { type: 'pattern', message: 'Solo se permiten letras' },
    ],
    dateStart: [
      { type: 'required', message: 'Email es requerido' },
      { type: 'email', message: 'example@example.com' },
    ],
  };
  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<CoursesComponent>,
    private userService: UserService,
    private courseService: CourseService,
    private _dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: IUser
  ) {
    this.date = new Date();
    this.buildForm();
  }

  buildForm(): void {
    this.dataForm = this.formBuilder.group({
      numberMonth: [1, [Validators.required]],
      dateStart: [this.date],
      dateEnd: [''],
      course: [[Validators.required]]
    });
  }

  ngOnInit(): void {
    this.getCourses();
    this.getCoursesToUser();
  }

  getCoursesToUser() {
    this.userService.getAllCourses(this.data)
      .subscribe(
        (res: IBuyedCourse[]) => {
          this.coursesByUser = res;
        }
      );
  }

  getCourses() {
    this.courseService.getAll().subscribe(
      (res: ICourse[]) => {
        this.courses = res;
      }
    );
  }

  closeBtn(): void {
    this.dialogRef.close();
  }

  addData(swal: SwalComponent): void {
    const dateStart: Date = this.dataForm.controls.dateStart.value;
    const numberMonth: number = this.dataForm.controls.numberMonth.value;
    const dateEnd: Date = moment(dateStart).add(numberMonth, 'M').toDate();
    this.dataForm.controls.dateEnd.setValue(dateEnd);
    const data: IBuyedCourse = this.dataForm.value;
    Swal.fire({
      title: '¿Está seguro de agregar el curso al usuario?',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Agregar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Guardando...!',
          html: 'Espere un momento, los datos se estan registrando',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading();
            this.userService.addCourses(data, this.data)
              .then(
                () => {
                  swal.fire();
                  this.dataForm.reset();
                }
              )
              .catch(
                (error: any) => {
                  console.log(error);
                  Swal.close();
                }
              )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }

  public approveCourse(event: MatCheckboxChange, buyedCourse: IBuyedCourse): void {
    const buyedCourseUpdate: IBuyedCourse = {
      ...buyedCourse,
      approved: event.checked
    };

    this.userService.approvedCourseByUser(buyedCourseUpdate, this.data);
  }

  addMonths(date: Date, months: number) {
      date.setMonth(date.getMonth() + months);
      return date;
  }

  public showVoucher(voucher: string): void {
    const dialogref: MatDialogRef<ShowFileComponent> = this._dialog.open(ShowFileComponent, {
        width: '500px',
        height:  '500px',
        panelClass: 'modal-form',
        data: voucher
    });
  }
}
