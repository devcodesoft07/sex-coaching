import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { MatDialog } from '@angular/material/dialog';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { IUser } from 'src/app/core/model/admin/user.model';
import { UserService } from 'src/app/core/service/admin/user/user.service';
import Swal from 'sweetalert2';
import { CoursesComponent } from '../../components/courses/courses.component';

@Component({
  selector: 'int-ero-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  @Input() dataSource: IUser[] = [];
  public displayedColumns: string[];
  constructor(private userService: UserService,
    private storage: AngularFireStorage,
    private dialog: MatDialog) {
    this.displayedColumns = ['user', 'email', 'phone', 'dateStart', 'action'];
  }

  ngOnInit(): void {
  }

  deleteData(swal: SwalComponent, data: IUser): void {
    Swal.fire({
      title: `¿Está seguro de eliminar al usuario ${data.name}?`,
      text: 'Una vez eliminado no se podrá recuperar los datos.',
      showCancelButton: true,
      icon: 'question',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      customClass: 'swalConfirm'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: '¡Eliminando...!',
          html: 'Espere un momento, estamos eliminando al usuario',
          allowEscapeKey: false,
          allowOutsideClick: false,
          timer: 1500,
          customClass: 'swalLoading',
          didOpen: () => {
            Swal.showLoading()
            this.userService.delete(data)
            .then(
              () => {
                swal.fire();
                if (data.avatar !== '' || data.avatar !== null ) {
                  this.storage.storage.refFromURL(data.avatar as string).delete();
                }
              }
            )
            .catch(
              (error: any) => {
                console.log(error);
                Swal.close();
              }
            )
          },
          willClose: () => {
            // clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
          }
        })
      } else if (result.isDenied) {
        // Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }


  courseGestion(user: IUser): void {
    const dialogRef = this.dialog.open( CoursesComponent, {
      width: '700px',
      height: '600px',
      panelClass: 'modal-form',
      data: user
    });
  }
}
