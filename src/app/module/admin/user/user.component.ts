import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { IUser } from 'src/app/core/model/admin/user.model';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';
import { AddUserComponent } from './components/add-user/add-user.component';
import { UserService } from 'src/app/core/service/admin/user/user.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  users!: IUser[];
  state: boolean = false;
  dataToSearch: string = '';
  user!: IUser;
  constructor(
    private userService: UserService,
    private breakpointObserver: BreakpointObserver,
    private dialogController: MatDialog,
    private cookieService: CookieService
  ) {
    this._initialize();
  }

  private _initialize() {
    if (this.cookieService.check(AuthConstans.USER)) {
      this.user = JSON.parse(this.cookieService.get(AuthConstans.USER));
    }
  }


  ngOnInit(): void {
    if (this.user?.role === 'ADMIN') {
      this.getAllData();
    }
  }

  getAllData() {
    this.state = true;
    this.userService.getAll()
      .subscribe(
        (res: IUser[]) => {
          this.state = false;
          this.users = res;
        },
        (error: any) => {
          this.state = false;
          console.log(error);
        }
      );
  }

  addNewData(): void {
    const dialogRef: MatDialogRef<AddUserComponent, any> = this.dialogController.open(AddUserComponent, {
      width: '600px',
      height: '600px',
      panelClass: 'modal-form',
    });
  }
}
