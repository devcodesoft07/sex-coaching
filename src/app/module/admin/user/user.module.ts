import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddUserComponent } from './components/add-user/add-user.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { UserListComponent } from './container/user-list/user-list.component';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AvatarModule } from 'ngx-avatar';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { CoursesComponent } from './components/courses/courses.component';


@NgModule({
  declarations: [
    UserComponent,
    AddUserComponent,
    EditUserComponent,
    UserListComponent,
    CoursesComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AvatarModule,
    SweetAlert2Module
  ]
})
export class UserModule { }
