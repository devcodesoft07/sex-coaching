import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IAboutUsProfile } from 'src/app/core/model/admin/about-us.model';
import { AboutUsService } from 'src/app/core/service/admin/about/about-us.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  constructor(private _aboutUsService: AboutUsService) {
  }

  ngOnInit(): void {
  }

}
