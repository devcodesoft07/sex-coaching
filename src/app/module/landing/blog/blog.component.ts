import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { IBlog } from 'src/app/core/model/admin/blog.model';
import { BlogService } from 'src/app/core/service/admin/blog/blog.service';
import Swiper, { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakPointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  config: SwiperOptions = {
    centeredSlides: true,
    navigation: true,
    loop: true,
    autoplay: true,
    pagination: { clickable: true },
    speed: 500
  };
  realIndexSwiper: number;
  public blogs$: Observable<IBlog[]>;
  constructor(
    private breakPointObserver: BreakpointObserver,
    private _blogService: BlogService
  ) {
    this.realIndexSwiper = 0;
    this.blogs$ = new Observable<IBlog[]>();
  }
  ngOnInit(): void {
    this._initialize();
  }

  private _initialize(): void {
    this._getBlogs();
  }

  private _getBlogs(): void {
    this.blogs$ = this._blogService.getAll();
  }
  verifyIndexActivate(swiper: Swiper[]): void {
    const swiperActive: Swiper = swiper[0];
    this.realIndexSwiper = swiperActive.realIndex;
  }

}
