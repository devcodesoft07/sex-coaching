import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SwiperModule } from 'swiper/angular';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { BlogCarrouselContainer } from './container/blog-carrousel/blog-carrousel.container';
import { BlogDataContainer } from './container/blog-data/blog-data.container';
import { AvatarModule } from 'ngx-avatar';


@NgModule({
  declarations: [
    BlogComponent,
    BlogCarrouselContainer,
    BlogDataContainer
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    SharedModule,
    SwiperModule,
    AngularMaterialModule,
    AvatarModule
  ]
})
export class BlogModule { }
