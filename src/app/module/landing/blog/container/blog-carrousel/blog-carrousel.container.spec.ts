import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogCarrouselContainer } from './blog-carrousel.container';

describe('BlogCarrouselComponent', () => {
  let component: BlogCarrouselContainer;
  let fixture: ComponentFixture<BlogCarrouselContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlogCarrouselContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogCarrouselContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
