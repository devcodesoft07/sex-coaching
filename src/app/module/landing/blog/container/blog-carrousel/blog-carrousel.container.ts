import { Component, Input, OnInit } from '@angular/core';
import { IBlog } from 'src/app/core/model/admin/blog.model';

@Component({
  selector: 'int-ero-blog-carrousel',
  templateUrl: './blog-carrousel.container.html',
  styleUrls: ['./blog-carrousel.container.scss']
})
export class BlogCarrouselContainer implements OnInit {
  @Input() index!: number;
  @Input() blog!: IBlog;
  @Input() realIndexSwiper!: number;
  constructor() { }

  ngOnInit(): void {
  }

}
