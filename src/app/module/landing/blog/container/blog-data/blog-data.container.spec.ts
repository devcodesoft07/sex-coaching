import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogDataContainer } from './blog-data.container';

describe('BlogDataComponent', () => {
  let component: BlogDataContainer;
  let fixture: ComponentFixture<BlogDataContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlogDataContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogDataContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
