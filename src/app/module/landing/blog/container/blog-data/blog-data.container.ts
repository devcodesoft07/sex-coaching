import { Component, OnInit, Input } from '@angular/core';
import { IBlog } from 'src/app/core/model/admin/blog.model';

@Component({
  selector: 'int-ero-blog-data',
  templateUrl: './blog-data.container.html',
  styleUrls: ['./blog-data.container.scss']
})
export class BlogDataContainer implements OnInit {
  @Input() blog!: IBlog;

  constructor() { }

  ngOnInit(): void {
  }

}
