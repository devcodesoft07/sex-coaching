import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConditionsComponent } from './conditions.component';
import { ConditionsRoutingModule } from './conditions-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ConditionsRoutingModule
  ],
  declarations: [ConditionsComponent]
})
export class ConditionsModule { }
