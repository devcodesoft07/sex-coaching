import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import emailjs, { EmailJSResponseStatus } from 'emailjs-com';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  dataForm!: FormGroup;
  state: boolean = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.dataForm = this.formBuilder.group({
      email: ['', [Validators.required]],
    });
  }

  sendDataIntoForm(): void {
    this.state = true;
    const template_params:any = {
      from_email: this.dataForm.controls.email.value,
    };
    if (this.dataForm.valid) {
      emailjs.send('service_ey3x625', 'template_kkd94i7', template_params, 'FQq4wQb5TCRUJshtV')
      .then((result: EmailJSResponseStatus) => {
        console.log(result.text);
        this.state = false;
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: '¡Envio exitóso!',
          text: 'Te contactaremos lo mas pronto posible',
          showConfirmButton: false,
          timer: 3000
        })
        this.dataForm.reset();
      }, (error) => {
        this.state = false;
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: '¡Oops, ocurrio un problema!',
          text: 'Comunícate con nosotros via whatsapp, +591 63383499',
          showConfirmButton: true,
          confirmButtonColor: '#2E7D32',
          // timer: 2000
        })
        console.log(error.text);
      });
    } else {
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Por favor llena los campos con (*)',
        text: 'El correo electronico debe seguir el siguiente formato ejemplo@ejemplo.com',
        showConfirmButton: true,
        confirmButtonColor: '#2E7D32',
        // timer: 2000
      })
      this.state = false;
    }

  }

}
