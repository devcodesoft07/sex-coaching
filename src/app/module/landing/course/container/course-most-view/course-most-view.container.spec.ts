import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseMostViewContainer } from './course-most-view.container';

describe('CourseMostViewComponent', () => {
  let component: CourseMostViewContainer;
  let fixture: ComponentFixture<CourseMostViewContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseMostViewContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseMostViewContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
