import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { ShoppingCartService } from 'src/app/core/service/shopping-card/shopping-card.service';

@Component({
  selector: 'app-course-container',
  templateUrl: './course.container.html',
  styleUrls: ['./course.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseContainer implements OnInit {
  @Input() course!: ICourse;
  constructor(
    private dialogController: MatDialog,
    public shoppingCartService: ShoppingCartService
  ) { }

  ngOnInit(): void {
  }

  public addCourseToShoppingCar(): void {
    this.shoppingCartService.addCourseToList(this.course);
  }

}
