import { CourseService } from 'src/app/core/service/admin/course/course.service';
import { Component, OnInit } from '@angular/core';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { FirebaseError } from '@angular/fire/app';
import { IVideo } from 'src/app/core/model/admin/video.model';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {

  public selected: string = 'Todos';
  public courses: ICourse[];
  public playlist!: IVideo[];
  public course: ICourse;

  constructor(
    private courseService: CourseService,
  ) {
    this.courses = [];
    this.course = {} as ICourse;
  }

  ngOnInit(): void {
    this._initialize();
  }

  private _initialize() {
    this._getCourseList();
  }
  private _getCourseList() {
    this.courseService.getAll().subscribe(
      (res: ICourse[]) => {
        this.courses = res;
        this.course = this.courses[0];
        this._getAllVideos(this.courses[0]);
      }
    );
  }

  private _getAllVideos(course: ICourse) {
    this.courseService.getAllVideos(course)
      .subscribe(
        (res: IVideo[]) => {
          this.playlist = res;
          console.log('Videos', res);
        },
        (error: FirebaseError) => {
          console.log(error);
        }
      );
  }
}
