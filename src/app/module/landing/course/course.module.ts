import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseRoutingModule } from './course-routing.module';
import { CourseComponent } from './course.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { AvatarModule } from 'ngx-avatar';
import { CourseContainer } from './container/course/course.container';
import { CourseMostViewContainer } from './container/course-most-view/course-most-view.container';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CourseDetailModule } from './page/course-detail/course-detail.module';


@NgModule({
  declarations: [
    CourseComponent,
    CourseContainer,
    CourseMostViewContainer,
  ],
  imports: [
    CommonModule,
    CourseRoutingModule,
    SharedModule,
    SweetAlert2Module,
    AngularMaterialModule,
    AvatarModule,
    FormsModule,
    ReactiveFormsModule,
    CourseDetailModule
  ]
})
export class CourseModule { }
