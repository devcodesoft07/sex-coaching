import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseDataListContainer } from './course-data-list.container';

describe('CourseDataListComponent', () => {
  let component: CourseDataListContainer;
  let fixture: ComponentFixture<CourseDataListContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseDataListContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseDataListContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
