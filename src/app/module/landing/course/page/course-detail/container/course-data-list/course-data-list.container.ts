import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { IUser } from 'src/app/core/model/admin/user.model';
import { IVideo } from 'src/app/core/model/admin/video.model';

@Component({
  selector: 'int-ero-course-data-list-view',
  templateUrl: './course-data-list.container.html',
  styleUrls: ['./course-data-list.container.scss']
})
export class CourseDataListContainer implements OnInit {
  @Input() video!: IVideo;
  @Input() user!: IUser;
  @Input() index!: number;
  @Input() videoSelected!: IVideo;
  @Output() selectEventEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output() actionEventEmitter: EventEmitter<string> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }


}
