import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ICourse } from 'src/app/core/model/admin/course.model';

@Component({
  selector: 'app-course-summary',
  templateUrl: './course-summary.component.html',
  styleUrls: ['./course-summary.component.scss']
})
export class CourseSummaryComponent implements OnInit {
  @Input() course: ICourse | undefined;
  @Input() hiddenContainer: boolean;
  @Output() clickedShoppingButtonEmitter: EventEmitter<any> = new EventEmitter();
  constructor() {
    this.hiddenContainer = false;
  }

  ngOnInit(): void {
  }

}
