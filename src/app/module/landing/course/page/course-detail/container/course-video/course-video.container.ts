import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { VgControlsHiddenService } from '@videogular/ngx-videogular/core';
import { IVideo } from 'src/app/core/model/admin/video.model';

@Component({
  selector: 'int-ero-course-video-view',
  templateUrl: './course-video.container.html',
  styleUrls: ['./course-video.container.scss']
})
export class CourseVideoContainer implements OnInit {
  @Input() video!: IVideo;
  @Input() videoIndex!: number;
  @Input() playlist!: IVideo[];
  @Output() btnEventEmitter: EventEmitter<any> = new EventEmitter();
  constructor(private controlsHidden: VgControlsHiddenService) { }

  ngOnInit(): void {
  }

  onPlayerReady(api: any) {
    // this.api = api;

    // this.api.getDefaultMedia().subscriptions.loadedMetadata.subscribe();
    // // this.api.getDefaultMedia().subscriptions.loadedMetadata.subscribe(this.playVideo.bind(this));

  }

  playVideo() {
    // this.api.play();
  }

  previousVideo(): void {
    const videoIndex = this.videoIndex - 1;
    const video = this.playlist[videoIndex];
    this.btnEventEmitter.emit({
      video: video,
      index: videoIndex
    });
  }

  nextVideo(): void {
    const videoIndex = this.videoIndex + 1;
    const video = this.playlist[videoIndex];
    this.btnEventEmitter.emit({
      video: video,
      index: videoIndex
    });
  }
}
