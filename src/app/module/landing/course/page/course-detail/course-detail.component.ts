import { Component, Input, OnInit } from '@angular/core';
import { FirebaseError } from '@angular/fire/app';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { IVideo } from 'src/app/core/model/admin/video.model';
import { CourseService } from 'src/app/core/service/admin/course/course.service';
import { ShoppingCartService } from 'src/app/core/service/shopping-card/shopping-card.service';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.scss']
})
export class CourseDetailComponent implements OnInit {
  @Input() courseInput!: ICourse;
  @Input() playlistInput: IVideo[];
  @Input() hiddenContainer: boolean;
  public id!: string;
  private _subscriptions: Subscription;
  public course!: ICourse & { id: string; } | undefined;
  public playlist!: IVideo[];
  public videoIndex = 0;
  public video!: IVideo;
  public state: boolean = false;
  constructor(
    private _router: Router,
    private _activeRoute: ActivatedRoute,
    private _courseService: CourseService,
    private _shoppingCartService: ShoppingCartService
  ) {
    // this.courseInput = {} as ICourse;
    this.playlistInput = {} as IVideo[];
    this.hiddenContainer = false;

    const activatedRouted$: Subscription = this._activeRoute.params.pipe(
      map(
        (res: Params) => {
          this.id = res.id;
          return res;
        }
      )
    ).subscribe();
    this._subscriptions = new Subscription();
    this._subscriptions.add(activatedRouted$);
  }

  ngOnInit(): void {
    this._getCourseDetaul();
    this.course = this.courseInput;
    this.playlist = this.playlistInput;
    this.video = this.playlist[0];
  }

  ngOnDestroy(): void {
    this._subscriptions.unsubscribe();
  }

  private _getCourseDetaul(): void {
    if (!this.courseInput) {
      this._courseService.getData(this.id).subscribe(
        (res: ICourse & { id: string; } | undefined) => {
          this.course = res;
          this._getAllVideos();
        },
        (error: FirebaseError) => {
          console.log(error);
        }
      );
    }
  }

  private _getAllVideos() {
    this._courseService.getAllVideos(this.course)
      .subscribe(
        (res: IVideo[]) => {
          this.playlist = res;
          this.video = res[this.videoIndex];
        },
        (error: FirebaseError) => {
          console.log(error);
        }
      );
  }

  onClickPlaylistItem(item: any, index: number) {
    this.videoIndex = index;
    this.video = item;
  }


  public goBack(): void {
    this._router.navigate(['/public/cursos/']);
  }

  public addCourseToShoppingCar(): void {
    this._shoppingCartService.addCourseToList(this.course as ICourse);
  }
}
