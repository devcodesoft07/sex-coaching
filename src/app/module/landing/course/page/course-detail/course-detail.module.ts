import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseDetailRoutingModule } from './course-detail-routing.module';
import { CourseDetailComponent } from './course-detail.component';
import { CourseSummaryComponent } from './container/course-summary/course-summary.component';
import { AboutCourseComponent } from './container/about-course/about-course.component';
import { CourseSyllabusComponent } from './container/course-syllabus/course-syllabus.component';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { AvatarModule } from 'ngx-avatar';
import { SharedModule } from 'src/app/shared/shared.module';
import { CourseDataListContainer } from './container/course-data-list/course-data-list.container';
import { CourseVideoContainer } from './container/course-video/course-video.container';
import { VgBufferingModule } from '@videogular/ngx-videogular/buffering';
import { VgControlsModule } from '@videogular/ngx-videogular/controls';
import { VgCoreModule } from '@videogular/ngx-videogular/core';
import { VgOverlayPlayModule } from '@videogular/ngx-videogular/overlay-play';


@NgModule({
  declarations: [
    CourseDetailComponent,
    CourseSummaryComponent,
    AboutCourseComponent,
    CourseSyllabusComponent,
    CourseDataListContainer,
    CourseVideoContainer
  ],
  imports: [
    CommonModule,
    CourseDetailRoutingModule,
    AngularMaterialModule,
    AvatarModule,
    SharedModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
  ],
  exports: [
    CourseDetailComponent
  ]
})
export class CourseDetailModule { }
