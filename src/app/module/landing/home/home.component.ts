import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos'
import { Observable } from 'rxjs';
import { IAboutUsBanner } from 'src/app/core/model/admin/about-us.model';
import { AboutUsService } from 'src/app/core/service/admin/about/about-us.service';
@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public banner$: Observable<IAboutUsBanner>;
  constructor(private _aboutUsService: AboutUsService) {
    this.banner$ = new Observable<IAboutUsBanner>();
  }

  ngOnInit(): void {
    AOS.init(
      {
        duration: 1200
      }
    );
    this._initialize();
  }

  private _initialize(): void {
    this._getBanner();
  }
  private _getBanner(): void {
    this.banner$ = this._aboutUsService.doGetBanner();
  }

}
