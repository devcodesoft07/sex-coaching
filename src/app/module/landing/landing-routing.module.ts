import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from './landing.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'inicio' },
      {
        path: 'inicio',
       loadChildren: () => import('../landing/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'cursos',
        // canActivate: [AuthGuard],
        loadChildren: () => import('../landing/course/course.module').then(m => m.CourseModule)
      },
      {
        path: 'blogs',
        loadChildren: () => import('../landing/blog/blog.module').then(m => m.BlogModule)
      },
      {
        path: 'nosotros',
        loadChildren: () => import('../landing/about-us/about-us.module').then(m => m.AboutUsModule)
      },
      {
        path: 'contacto',
        loadChildren: () => import('../landing/contact/contact.module').then(m => m.ContactModule)
      },
      {
        path: 'condiciones',
        loadChildren: () => import('../landing/conditions/conditions.module').then(m => m.ConditionsModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
