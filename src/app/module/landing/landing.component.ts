import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MenuService } from 'src/app/core/service/menu/menu.service';
import { Link } from 'src/app/shared/components/navbar/navbar.component';

@Component({
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements AfterViewInit {
  title = 'sex-coaching';
  links: Link[] = [
    {
      route: 'inicio',
      label: 'Inicio'
    },
    {
      route: 'cursos',
      label: 'Cursos'
    },
    {
      route: 'blogs',
      label: 'Blogs'
    },
    {
      route: 'nosotros',
      label: 'Nosotros'
    },
    {
      route: 'contacto',
      label: 'Contacto'
    },
    // {
    //   route: 'condiciones',
    //   label: 'Terminos y Condiciones'
    // }
  ];

  linksSocialNetworks: Link[];
  @ViewChild('sidenav') public sidenav!: MatSidenav;
  @ViewChild('shoppingSidenav') public shoppingSidenav!: MatSidenav;

  constructor(
    private sidenavService: MenuService
  ) {
    this.linksSocialNetworks = [
      {
        route: 'https://wa.me/76938377',
        label: 'Whatsapp',
        icon: 'assets/icons/whatsapp.svg'
      },
      {
        route: 'https://www.facebook.com/messages/t/273548262769558',
        label: 'Messenger',
        icon: 'assets/icons/messenger.svg'
      },
      // {
      //   route: 'https://www.facebook.com/DoctorOquendoM',
      //   label: 'Facebook / Dr. Manuel Oquendo',
      //   icon: 'assets/icons/facebook.svg'
      // },
      {
        route: 'https://www.tiktok.com/@inteligenciaerotica',
        label: 'Tiktok / Inteligencia Erótica',
        icon: 'assets/icons/tiktok.svg'
      },
      {
        route: 'https://www.instagram.com/inteligenciaerotica/?igshid=YmMyMTA2M2Y%3D',
        label: 'Instagram / Inteligencia Erótica',
        icon: 'assets/icons/instagram.svg'
      },
      {
        route: 'https://www.facebook.com/inteligenciaerotica.cochabamba',
        label: 'Facebook / Inteligencia Erótica',
        icon: 'assets/icons/facebook.svg'
      },
    ];
  }

  ngAfterViewInit(): void {
  }

  closeSideNav(): void {
    this.sidenavService.setSidenav(this.sidenav);
    setTimeout(() => {
      this.sidenavService.toggle();
    }, 300);
  }

  shoppingClick(): void {
    this.sidenavService.setSidenav(this.shoppingSidenav);
    setTimeout(() => {
      this.sidenavService.toggle();
    }, 300);
  }

  menuClick(): void {
    this.sidenavService.setSidenav(this.sidenav);
    setTimeout(() => {
      this.sidenavService.toggle();
    }, 300);
  }

}
