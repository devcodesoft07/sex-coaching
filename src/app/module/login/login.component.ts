import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { CookieService } from 'ngx-cookie-service';
// import { CookieService } from 'ngx-cookie-service';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';
import { IAuthUser } from 'src/app/core/model/user.model';
import { AuthService } from 'src/app/core/service/auth/auth.service';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import firebase from 'firebase/compat/app';
import { FirebaseError } from '@angular/fire/app';
import { ERole, IUser } from 'src/app/core/model/admin/user.model';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { UserService } from 'src/app/core/service/admin/user/user.service';
import { UserMetadata } from 'firebase/auth';
import { MessageService } from 'src/app/core/service/message/message.service';
import { VariablesConstanst } from 'src/app/core/config/variables.config';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm!: FormGroup;
  public checked: boolean;
  public get email(): AbstractControl | null {
    return this.loginForm.get('email');
  }
  public get password(): AbstractControl | null {
    return this.loginForm.get('password');
  }

  public errorsMessages = {
    email: [
      { type: 'required', message: 'Email es requerido' },
      { type: 'email', message: 'El email sigue el siguiente formato ejemplo@ejemplo.com' },
    ],
    password: [
      { type: 'required', message: 'Contraseña es requerido' },
      { type: 'minlength', message: 'La contraseña debe contener minimo 8 caracateres' },
    ]
  }

  private _subscribers: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private cookieService: CookieService,
    private userService: UserService,
    private messageService: MessageService
  ) {
    this._subscribers = new Subscription();
    this.checked = false;
    this.buildForm();
    this._initialize();
  }

  ngOnDestroy(): void {
    this._subscribers.unsubscribe();
  }

  private _initialize() {
    this._getCredentialsFromStorage();
  }

  private _getCredentialsFromStorage() {
    const credentiallFromStorage = localStorage.getItem(AuthConstans.CREDENTIALS);
    if (credentiallFromStorage) {
      const credentials: IAuthUser = JSON.parse(credentiallFromStorage) as IAuthUser;
      this.loginForm.patchValue(credentials);
      this.checked = true;
    }
  }

  ngOnInit(): void {
  }

  buildForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  public rememberCheck(event: MatCheckboxChange): void {
    const isChecked: boolean = event.checked;
    if (isChecked) {
      const loginFormValue: IAuthUser = this.loginForm.value as IAuthUser;
      localStorage.setItem(AuthConstans.CREDENTIALS, JSON.stringify(loginFormValue));
    }
  }


  login(swalSuccess: SwalComponent): void {
    const data: IAuthUser = this.loginForm.value;
    Swal.fire({
      title: 'Iniciando Sesión...!',
      html: 'Espere un momento, los datos se estan validando',
      allowEscapeKey: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading()
        this.authService.loginFire(data)
          .then(
            (res: firebase.auth.UserCredential) => {
              this.cookieService.set(AuthConstans.AUTH, JSON.stringify(res.user));
              swalSuccess.fire();
              this._getUserById(res);
              // this.cookieService.set(AuthConstans.USER, JSON.stringify(res.user));
            },
            (error: FirebaseError) => {
              let message: string = '';
              if (error.code === 'auth/wrong-password' || error.code === 'auth/invalid-credential' || error.code === 'auth/invalid-email' || error.code === 'auth/user-not-found') {
                message = 'Correo electronico / contraseña incorrectas';
              }
              Swal.close();
              Swal.fire({
                icon: 'error',
                title: 'Oops... error',
                text: message,
                timer: 2000,
              });
            }
          );
      },
      willClose: () => {
        // clearInterval(timerInterval)
      }
    }).then((result) => {
    })
  }
  private _routerNavigation(): void {
    const isPaymentWay: string = localStorage.getItem(VariablesConstanst.PAYMENT_WAY) as string;
    if (isPaymentWay) {
      if (isPaymentWay === 'true') {
        this.goToSecureRoute('/secure/carrito');
      } else {
        this.goToSecureRoute('/secure');
      }
    } else {
      this.goToSecureRoute('/secure');
    }
  }

  private goToSecureRoute(route: string): void {
    this.router.navigateByUrl('secure', { skipLocationChange: true }).then(
      () => {
        this.router.navigate([route]);
      }
    )
  }

  private _getUserById(res: firebase.auth.UserCredential): void {
    const user$: Subscription = this.authService.getUserByUid(res.user?.uid)
      .subscribe(
        (res: IUser | undefined) => {
          this.cookieService.set(AuthConstans.USER, JSON.stringify(res));
          this._routerNavigation();
          Swal.close();
        }
      );
    this._subscribers.add(user$);
  }

  public signInWithGoogle(): void {
    this.authService.signInWithGoogle()
      .then(
        (res: firebase.auth.UserCredential) => {
          this.cookieService.set(AuthConstans.AUTH, JSON.stringify(res.user));
          this._registerNewUser(res);
        }
      )
      .catch(
        (error: FirebaseError) => {
          console.log(error);
        }
      );
  }
  private _registerNewUser(userCredentials: firebase.auth.UserCredential): void {
    const metadataTime: UserMetadata = userCredentials.user?.metadata as UserMetadata;
    const data: IUser = {
      id: userCredentials.user?.uid as string,
      name: userCredentials.user?.displayName as string,
      role: ERole.SUBSCRIBER,
      avatar: userCredentials.user?.photoURL as string,
      country: JSON.stringify(metadataTime),
      email: userCredentials.user?.email as string,
      dateStart: new Date(),
      password: ''
    };
    Swal.fire({
      title: 'Iniciando Sesión...!',
      html: 'Espere un momento, los datos se estan validando',
      allowEscapeKey: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
        this.userService.add(data, data.id as string)
          .then(
            () => {
              this._getUserById(userCredentials);
              const title: string = '¡Bienvenido!';
              const icon: SweetAlertIcon = 'success';
              this.messageService.showMessageConfirmation(title, '', 'center', icon, 2000);
            }
          )
          .catch(
            (error: FirebaseError) => {
              console.log(error);
            }
          )
      },
      willClose: () => {
        // clearInterval(timerInterval)
      }
    }).then((result) => {
    })
  }

}
