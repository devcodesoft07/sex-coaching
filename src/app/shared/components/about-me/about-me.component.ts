import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IAboutUsProfile } from 'src/app/core/model/admin/about-us.model';
import { AboutUsService } from 'src/app/core/service/admin/about/about-us.service';
@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss']
})
export class AboutMeComponent implements OnInit {
  labels: string[] = [
    'Medico',
    'Psicologo',
    'Ginecologo',
    'Obstetra',
    'Sexcoach'
  ]
  public profile$: Observable<IAboutUsProfile>;
  route?: string;
  constructor(private router: Router,
    private _aboutUsService: AboutUsService) {

      this.profile$ = new Observable<IAboutUsProfile>();

  }

  ngOnInit(): void {
    this.route = this.router.url;
    this._getAboutUsProfile();
  }

  get routeAll() {
    return this.route ? this.route : null
  };

  private _getAboutUsProfile(): void {
    this.profile$ = this._aboutUsService.doGetProfile();
  }
}
