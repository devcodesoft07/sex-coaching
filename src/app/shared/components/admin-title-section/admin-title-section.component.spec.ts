import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTitleSectionComponent } from './admin-title-section.component';

describe('AdminTitleSectionComponent', () => {
  let component: AdminTitleSectionComponent;
  let fixture: ComponentFixture<AdminTitleSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminTitleSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTitleSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
