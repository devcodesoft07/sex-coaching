import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-title-section',
  templateUrl: './admin-title-section.component.html',
  styleUrls: ['./admin-title-section.component.scss']
})
export class AdminTitleSectionComponent implements OnInit {
  @Input() title!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
