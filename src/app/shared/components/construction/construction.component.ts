import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-construction',
  templateUrl: './construction.component.html',
  styleUrls: ['./construction.component.scss']
})
export class ConstructionComponent implements OnInit {
  @Input() title!: string;
  @Input() mode: string = 'light';
  constructor() { }

  ngOnInit(): void {
  }

}
