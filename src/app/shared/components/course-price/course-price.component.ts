import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'int-ero-course-price',
  templateUrl: './course-price.component.html',
  styleUrls: ['./course-price.component.scss']
})
export class CoursePriceComponent implements OnInit {
  @Input() showCardPrice: boolean = false;
  @Input() price: number | undefined = 350;
  @Input() titleCourse: string  | undefined = 'Sexo Gourmet';
  @Output() clickedShoppingButtonEmitter: EventEmitter<any> = new EventEmitter();
  dataSource: string[];
  displayedColumns: string[];
  constructor() {
    this.dataSource = [
      'Acceso al curso que compres por un mes',
      'Contenido exclusivo ',
      'Acceso a la plataforma 24/7',
      'Resolución de dudas por correo al finalizar el curso',
    ];
    this.displayedColumns = [
      'detail',
      'check'
    ]
  }

  ngOnInit(): void {
  }

}
