import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ERole, IUser } from 'src/app/core/model/admin/user.model';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';

@Component({
  selector: 'int-ero-empty-state',
  templateUrl: './empty-state.component.html',
  styleUrls: ['./empty-state.component.scss']
})
export class EmptyStateComponent implements OnInit {
  @Input() btnTitle!: string;
  @Input() title: string = 'No existen datos';
  @Input() text: string = '¡Ups, no existen datos registrados.!';
  @Output() btnEventEmitter: EventEmitter<boolean> = new EventEmitter();
  user!: IUser;
  role = ERole;
  constructor(
    private cookieService: CookieService
  ) {
    this._initialize();
  }

  private _initialize() {
    if (this.cookieService.check(AuthConstans.USER)) {
      this.user = JSON.parse(this.cookieService.get(AuthConstans.USER));
    }
  }

  ngOnInit(): void {
  }

}
