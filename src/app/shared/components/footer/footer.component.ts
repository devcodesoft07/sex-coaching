import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import emailjs, { EmailJSResponseStatus } from 'emailjs-com';
import Swal from 'sweetalert2';

import { Link } from '../navbar/navbar.component';

@Component({
  selector: 'int-ero-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FooterComponent implements OnInit {
  public links: Link[];
  public socialLinks: Link[];
  public contactNetworks: Link[];
  dataForm!: FormGroup;
  state: boolean = false;
  constructor(private formBuilder: FormBuilder) {
    this.links = [
      {
        route: 'inicio',
        label: 'Inicio'
      },
      {
        route: 'cursos',
        label: 'Cursos'
      },
      {
        route: 'blogs',
        label: 'Blogs'
      },
      {
        route: 'nosotros',
        label: 'Nosotros'
      },
      {
        route: 'contacto',
        label: 'Contacto'
      },
      {
        route: 'condiciones',
        label: 'Términos y Condiciones'
      }
    ];
    this.socialLinks = [
      {
        route: 'https://www.tiktok.com/@inteligenciaerotica',
        label: 'Tiktok / Inteligencia Erótica',
        icon: 'assets/icons/tiktok.svg'
      },
      {
        route: 'https://www.instagram.com/inteligenciaerotica/?igshid=YmMyMTA2M2Y%3D',
        label: 'Instagram / Inteligencia Erótica',
        icon: 'assets/icons/instagram.svg'
      },
      {
        route: 'https://www.facebook.com/inteligenciaerotica.cochabamba',
        label: 'Facebook / Inteligencia Erótica',
        icon: 'assets/icons/facebook.svg'
      },
    ];
    this.contactNetworks = [
      {
        route: 'https://wa.me/76938377',
        label: 'Whatsapp',
        icon: 'assets/icons/whatsapp.svg'
      },
      {
        route: 'https://www.facebook.com/messages/t/273548262769558',
        label: 'Messenger',
        icon: 'assets/icons/messenger.svg'
      },
    ];
  }

  ngOnInit(): void {
    this.dataForm = this.formBuilder.group({
      email: ['', [Validators.required]],
    });
  }

  sendDataIntoForm(): void {
    this.state = true;
    const template_params:any = {
      from_email: this.dataForm.controls.email.value,
    };
    if (this.dataForm.valid) {
      emailjs.send('service_ey3x625', 'template_kkd94i7', template_params, 'FQq4wQb5TCRUJshtV')
      .then((result: EmailJSResponseStatus) => {
        console.log(result.text);
        this.state = false;
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: '¡Envio exitóso!',
          text: 'Te contactaremos lo mas pronto posible',
          showConfirmButton: false,
          timer: 3000
        })
        this.dataForm.reset();
      }, (error) => {
        this.state = false;
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: '¡Oops, ocurrio un problema!',
          text: 'Comunícate con nosotros via whatsapp, +591 63383499',
          showConfirmButton: true,
          confirmButtonColor: '#2E7D32',
          // timer: 2000
        })
        console.log(error.text);
      });
    } else {
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Por favor llena los campos con (*)',
        text: 'El correo electronico debe seguir el siguiente formato ejemplo@ejemplo.com',
        showConfirmButton: true,
        confirmButtonColor: '#2E7D32',
        // timer: 2000
      })
      this.state = false;
    }

  }

}
