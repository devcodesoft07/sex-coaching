import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LearningUsComponent } from './learning-us.component';

describe('LearningUsComponent', () => {
  let component: LearningUsComponent;
  let fixture: ComponentFixture<LearningUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LearningUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LearningUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
