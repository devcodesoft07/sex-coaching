import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IAboutUsCallToAction } from 'src/app/core/model/admin/about-us.model';
import { AboutUsService } from 'src/app/core/service/admin/about/about-us.service';

@Component({
  selector: 'app-learning-us',
  templateUrl: './learning-us.component.html',
  styleUrls: ['./learning-us.component.scss']
})
export class LearningUsComponent implements OnInit {

  public aboutUsCallToAction!: IAboutUsCallToAction;
  constructor(private _aboutUsServices: AboutUsService) {
  }

  ngOnInit(): void {
    this._getDataCallToAction();
  }

  private _getDataCallToAction(): void {
    this._aboutUsServices.doGetCallToAction()
    .subscribe(
      ((res: IAboutUsCallToAction) => {
        this.aboutUsCallToAction = res;
      })
    );
  }

}
