import { Component, Input, OnInit } from '@angular/core';
import { IAboutUsCallToAction } from 'src/app/core/model/admin/about-us.model';
import { AboutUsService } from 'src/app/core/service/admin/about/about-us.service';

@Component({
  selector: 'app-learning',
  templateUrl: './learning.component.html',
  styleUrls: ['./learning.component.scss']
})
export class LearningComponent implements OnInit {
  @Input() showButton: boolean ;
  public aboutUsCallToAction!: IAboutUsCallToAction;
  constructor(
    private _aboutUsServices: AboutUsService,
  ) {
    this.showButton = true;
  }

  ngOnInit(): void {
    this._getDataCallToAction();
  }

  private _getDataCallToAction(): void {
    this._aboutUsServices.doGetCallToAction()
      .subscribe(
        ((res: IAboutUsCallToAction) => {
          this.aboutUsCallToAction = res;
        })
      );
  }

}
