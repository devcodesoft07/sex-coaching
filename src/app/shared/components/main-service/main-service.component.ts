import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IMainService } from 'src/app/core/model/main-service.model';
import { MainServiceService } from 'src/app/core/service/main-service/main-service.service';
@Component({
  selector: 'app-main-service',
  templateUrl: './main-service.component.html',
  styleUrls: ['./main-service.component.scss']
})
export class MainServiceComponent implements OnInit {
  public services$: Observable<IMainService[]>;

  constructor(
    private mainService: MainServiceService
  ) {
    this.services$ = new Observable<IMainService[]>();
  }

  ngOnInit(): void {
    this._getAllMainService();
  }

  private _getAllMainService() {
    this.services$ = this.mainService.doGetServices();
  }

}
