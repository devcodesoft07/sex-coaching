import { CookieService } from 'ngx-cookie-service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/service/auth/auth.service';
import { MenuService } from 'src/app/core/service/menu/menu.service';
import { Link } from '../navbar/navbar.component';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';
import { VariablesConstanst } from 'src/app/core/config/variables.config';
import { Observable } from 'rxjs';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { ShoppingCartService } from 'src/app/core/service/shopping-card/shopping-card.service';
import { IUser } from 'src/app/core/model/admin/user.model';

@Component({
  selector: 'app-navbar-admin',
  templateUrl: './navbar-admin.component.html',
  styleUrls: ['./navbar-admin.component.scss']
})
export class NavbarAdminComponent implements OnInit {
  activeLink!: Link;
  public courseList$: Observable<ICourse[]>;
  user!: IUser;

  constructor(
    private menuService: MenuService,
    private authService: AuthService,
    private router: Router,
    private cookieService: CookieService,
    private shoppingCartService: ShoppingCartService
  ) {
    this.courseList$ = this.shoppingCartService.coursList$;
  }


  ngOnInit(): void {
    this._initialize();
  }

  private _initialize(): void {
    if (this.cookieService.check(AuthConstans.USER)) {
      this.user = JSON.parse(this.cookieService.get(AuthConstans.USER)) as IUser;
    }
  }

  clickMenu() {
    this.menuService.toggle();
  }

  changeLink(link: Link, index: number): void {
    this.activeLink = link;
    localStorage.setItem('activateLink', index.toString())
  }

  logout(){
    this.authService.logoutFire()
    .then(
      () => {
        this.cookieService.delete(AuthConstans.AUTH);
        this.cookieService.delete(AuthConstans.USER);
        localStorage.setItem(VariablesConstanst.PAYMENT_WAY, 'false');
        this.router.navigate(['/public']);
      }
    )
    .catch();
  }

}
