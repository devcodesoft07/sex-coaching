import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Observable } from 'rxjs';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { MenuService } from 'src/app/core/service/menu/menu.service';
import { ShoppingCartService } from 'src/app/core/service/shopping-card/shopping-card.service';
export interface Link {
  route: string;
  label: string;
  icon?: string;
}
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit {
  @Output() clickShoppingEventEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() clickMenuEventEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();
  links: Link[] = [
    {
      route: 'inicio',
      label: 'Inicio'
    },
    {
      route: 'cursos',
      label: 'Cursos'
    },
    {
      route: 'blogs',
      label: 'Blogs'
    },
    {
      route: 'nosotros',
      label: 'Nosotros'
    },
    {
      route: 'contacto',
      label: 'Contacto'
    },
  ];

  public courseList$: Observable<ICourse[]>;
  constructor(
    private menuService: MenuService,
    private shoppingCartService: ShoppingCartService
  ) {
    this.courseList$ = this.shoppingCartService.coursList$;
  }

  ngOnInit(): void {
  }
  clickMenu() {
    this.menuService.toggle();
  }
}
