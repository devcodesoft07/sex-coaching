import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkSocialComponent } from './network-social.component';

describe('NetworkSocialComponent', () => {
  let component: NetworkSocialComponent;
  let fixture: ComponentFixture<NetworkSocialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetworkSocialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
