import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'int-ero-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.scss']
})
export class ShowFileComponent implements OnInit {

  constructor(
    private _dialogRef: MatDialogRef<ShowFileComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string
  ) { }

  ngOnInit(): void {
  }

  public closeModal(): void {
    this._dialogRef.close()
  }

}
