import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-route-section',
  templateUrl: './route-section.component.html',
  styleUrls: ['./route-section.component.scss']
})
export class RouteSectionComponent implements OnInit {
  @Input() routes!: string[];
  @Input() showBackButton: boolean = false;
  @Input() showCloseButton: boolean = false;
  @Output() btnEventEmitter: EventEmitter<boolean> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
}
