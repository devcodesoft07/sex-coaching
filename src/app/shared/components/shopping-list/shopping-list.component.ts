import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { VariablesConstanst } from 'src/app/core/config/variables.config';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { AuthConstans } from 'src/app/core/model/config/auth-constanst.config';
import { ShoppingCartService } from 'src/app/core/service/shopping-card/shopping-card.service';

@Component({
  selector: 'int-ero-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShoppingListComponent implements OnInit {
  @Input() showPaymentButton: boolean = true;
  @Output() closeEventEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();
  public courses$: Observable<ICourse[]>;
  constructor( private shoppingCartService: ShoppingCartService,
    private router: Router,
    private coockiesService: CookieService) {
    this.courses$ = this.shoppingCartService.coursList$;
  }

  ngOnInit(): void {
  }

  public deleteCourseToList(course: ICourse): void {
    this.shoppingCartService.deleteCourseToList(course);
  }

  public goToPayment(): void {
     const hasUserLoged: boolean = this.coockiesService.check(AuthConstans.AUTH);

    if (hasUserLoged) {
      this.router.navigate(['secure/carrito']);
    } else {
      this.router.navigate(['login']);
      localStorage.setItem(VariablesConstanst.PAYMENT_WAY, 'true');
    }
  }
}
