import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ILinks, ILinksCO } from 'src/app/core/model/links-co.model';
import { GlobalService } from 'src/app/core/service/links/global.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  links!: Observable<ILinksCO[]>;

  constructor(
    private linkService: GlobalService
  ) { }

  ngOnInit(): void {
    this.getLinkAdmin();
  }
  getLinkAdmin(): void {
    this.links = this.linkService.getLinks().pipe(map((res: ILinks) => {
      return res.links_admin;
    }));
  }

}
