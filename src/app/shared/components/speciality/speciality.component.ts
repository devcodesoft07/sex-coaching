import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ISpeciality } from 'src/app/core/model/admin/speciality.model';
import { SpecialityService } from 'src/app/core/service/admin/speciality/speciality.service';
import SwiperCore, { Autoplay, Navigation, Pagination, SwiperOptions } from 'swiper';

SwiperCore.use([
  Navigation,
  Autoplay,
  Pagination
]);
@Component({
  selector: 'app-speciality',
  templateUrl: './speciality.component.html',
  styleUrls: ['./speciality.component.scss']
})
export class SpecialityComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakPointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  config: SwiperOptions = {
    navigation: true,
    autoplay: true,
    pagination: { clickable: true },
  };
  specialities!: ISpeciality[];
  state: boolean = false;
  constructor(
    private specialityService: SpecialityService,
    private breakPointObserver: BreakpointObserver
  ) { }

  ngOnInit(): void {
    this.getAllData();
  }

  getAllData() {
    this.state = true;
    this.specialityService.getAll()
    .subscribe(
      (res: ISpeciality[]) => {
        this.state = false;
        this.specialities = res;
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      }
    );
  }

}
