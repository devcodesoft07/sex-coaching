import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ITestimonial } from 'src/app/core/model/admin/testimonial.model';
import { TestimonialService } from 'src/app/core/service/admin/testimonial/testimonial.service';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-testimony',
  templateUrl: './testimony.component.html',
  styleUrls: ['./testimony.component.scss']
})
export class TestimonyComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakPointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  config: SwiperOptions = {
    navigation: true,
    autoplay: true,
    pagination: { clickable: true },
  };
  public tesminonials$: Observable<ITestimonial[]>;
  constructor(
    private breakPointObserver: BreakpointObserver,
    private _testimonialService: TestimonialService
  ) {
    this.tesminonials$ = new Observable<ITestimonial[]>();
  }

  ngOnInit(): void {
    this._initialize();
  }

  private _initialize(): void {
    this._getTestimonials();
  }

  private _getTestimonials(): void {
    this.tesminonials$ = this._testimonialService.getAll();
  }

}
