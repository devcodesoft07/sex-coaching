import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { MessageService } from 'src/app/core/service/message/message.service';
import { UtilsService } from 'src/app/core/service/utils/utils.service';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'sex-coach-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UploadFileComponent implements OnInit {
  @Input() folderName: string;
  @Input() textButton: string;
  @Output() imageEventEmitter: EventEmitter<string>;
  public imageSelectedByUser: string;
  public file!: File | null | undefined;

  public uploadPercent!: Observable<number | undefined>;
  public statusUpload: boolean;
  public downloadURL!: Observable<string | undefined>;
  private _swalProperties: SweetAlertOptions;

  constructor(private _messageService: MessageService,
    private _angularFireStorage: AngularFireStorage,
    private _utilsService: UtilsService) {
    this.imageEventEmitter   = new EventEmitter<string>();
    this.imageSelectedByUser = 'assets/icon/empty-state/file.svg';
    this.folderName          = 'cotel-app';
    this._swalProperties = {
      title: 'Subido correctamente',
      text: '¡El archivo se subió correctamente!',
      icon: 'success',
      timer: 2000,
      showConfirmButton: false
    };
    this.statusUpload = false;
    this.textButton = 'Subir imagen';
  }

  ngOnInit(): void {
  }

  public previewImageSeleced(event: Event): void {
    const inputElement: HTMLInputElement = event.target as HTMLInputElement;
    const reader: FileReader = new FileReader();
    const files: FileList = inputElement.files as FileList;
    if (files && files.length) {
      this.file = files[0];
      reader.readAsDataURL(this.file);
      reader.onload = () => {
        this.imageSelectedByUser = reader.result as string;
      };
    }
  }

  public startedUploadFile(): void {
    const propertiesToShow: SweetAlertOptions = {
      title: '¿Estas seguro de subir la imagen seleccionada?',
      text: '',
      confirmButtonText: 'Subir',
      cancelButtonText: 'Cancel'
    }
    this._messageService.showSwalConfirmAlert(propertiesToShow)
      .subscribe(
        (res: boolean) => {
          if (res) {
            this.statusUpload = true;
            this._uploadFile();
          }
        }
      );
  }

  private _uploadFile(): void {
    const filePath = `${this.folderName}/${this._utilsService.makeId()}`;
    const fileRef = this._angularFireStorage.ref(filePath);
    const task = this._angularFireStorage.upload(filePath, this.file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
      finalize(() => {
        this.downloadURL = fileRef.getDownloadURL();
        this._sendUrlFile();
      })
    ).subscribe();
  }

  private _sendUrlFile(): void {
    this.downloadURL.subscribe(
      (res: string | undefined) => {
        this.statusUpload = false;
        this._messageService.showSwalBasicAlert(this._swalProperties);
        this.imageEventEmitter.emit(res);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
}
