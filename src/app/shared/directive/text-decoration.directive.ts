import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[intEroTextDecoration]'
})
export class TextDecorationDirective {
  @Input() text!: ElementRef | any;
  constructor(private renderer: Renderer2) { }


  @HostListener('mouseover')
  onMouseOver() {
    // this.renderer.setStyle(this.text, 'text-decoration', 'underline');
    this.renderer.addClass(this.text, 'text-decoration__content');
  }

  @HostListener('mouseout')
  onMouseOut() {
    // this.renderer.setStyle(this.text, 'text-decoration', 'none');
    this.renderer.removeClass(this.text, 'text-decoration__content');
  }
}
