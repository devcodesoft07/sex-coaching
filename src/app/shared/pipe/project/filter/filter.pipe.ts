import { Pipe, PipeTransform } from '@angular/core';
import { ICourse } from 'src/app/core/model/admin/course.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(dataList: ICourse[], textToFilter: string): ICourse[] {
    const text = textToFilter.toLowerCase();
    if (text === '') {
      return dataList;
    } else {
      return dataList.filter(
        (data: ICourse) => {
            return data.category.toLowerCase().includes(text);
        }
      );
    }
  }

}
