import { Pipe, PipeTransform } from '@angular/core';
import { ICourse } from 'src/app/core/model/admin/course.model';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(dataList: ICourse[], textToSearch: string): ICourse[] {
    const text = textToSearch.toLowerCase();
    if (text === '') {
      return dataList;
    } else {
      return dataList.filter(
        (data: ICourse) => {
            return data.title.toLowerCase().includes(text);
        }
      );
    }
  }

}
