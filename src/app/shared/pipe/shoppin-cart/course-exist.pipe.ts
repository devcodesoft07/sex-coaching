import { Pipe, PipeTransform } from '@angular/core';
import { ICourse } from 'src/app/core/model/admin/course.model';
import { ShoppingCartService } from 'src/app/core/service/shopping-card/shopping-card.service';

@Pipe({
  name: 'courseExist',
  pure: false
})
export class CourseExistPipe implements PipeTransform {

  constructor( private shoppingCartService: ShoppingCartService) {

  }

  transform(course: ICourse): boolean {
    const thereisACourse: boolean = this.shoppingCartService.verifyIsExistCourseOnCart(course);
    return thereisACourse;
  }

}
