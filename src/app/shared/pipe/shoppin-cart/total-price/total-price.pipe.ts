import { Pipe, PipeTransform } from '@angular/core';
import { ICourse } from 'src/app/core/model/admin/course.model';

@Pipe({
  name: 'totalPrice',
  pure: false
})
export class TotalPricePipe implements PipeTransform {

  transform(courseList: ICourse[] | null): number {
    const priceList: number[] = courseList?.map((course: ICourse) => course.price / 2) as number[];
    return priceList !== undefined ? priceList.reduce((previousPrice: number, nextPrice: number) => previousPrice + nextPrice, 0) : 0;
  }

}
