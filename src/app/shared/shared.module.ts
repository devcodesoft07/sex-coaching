import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

//External Module
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { SwiperModule } from 'swiper/angular';
//Components
import { NavbarComponent } from './components/navbar/navbar.component';
import { ConstructionComponent } from './components/construction/construction.component';
import { MainServiceComponent } from './components/main-service/main-service.component';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { SpecialityComponent } from './components/speciality/speciality.component';
import { LearningComponent } from './components/learning/learning.component';
import { TitleSectionComponent } from './components/title-section/title-section.component';
import { TestimonyComponent } from './components/testimony/testimony.component';
import { AvatarModule } from 'ngx-avatar';
import { OurHistoryComponent } from './components/our-history/our-history.component';
import { LearningUsComponent } from './components/learning-us/learning-us.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { SearchPipe } from './pipe/project/search/search.pipe';
import { FilterPipe } from './pipe/project/filter/filter.pipe';
import { RouteSectionComponent } from './components/route-section/route-section.component';
import { AdminTitleSectionComponent } from './components/admin-title-section/admin-title-section.component';
import { NavbarAdminComponent } from './components/navbar-admin/navbar-admin.component';
import { EmptyStateComponent } from './components/empty-state/empty-state.component';
import { NetworkSocialComponent } from './components/network-social/network-social.component';
import { CoursePriceComponent } from './components/course-price/course-price.component';
import { TextDecorationDirective } from './directive/text-decoration.directive';
import { CourseExistPipe } from './pipe/shoppin-cart/course-exist.pipe';
import { ShoppingListComponent } from './components/shopping-list/shopping-list.component';
import { TotalPricePipe } from './pipe/shoppin-cart/total-price/total-price.pipe';
import { ShowFileComponent } from './components/qr-code/qr-code.component';
import { UploadFileComponent } from './components/upload-file/upload-file.component';
import { FooterComponent } from './components/footer/footer.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NavbarComponent,
    ConstructionComponent,
    MainServiceComponent,
    AboutMeComponent,
    SpecialityComponent,
    LearningComponent,
    TitleSectionComponent,
    TestimonyComponent,
    OurHistoryComponent,
    LearningUsComponent,
    SidenavComponent,
    SearchPipe,
    FilterPipe,
    RouteSectionComponent,
    AdminTitleSectionComponent,
    NavbarAdminComponent,
    EmptyStateComponent,
    NetworkSocialComponent,
    CoursePriceComponent,
    TextDecorationDirective,
    CourseExistPipe,
    ShoppingListComponent,
    TotalPricePipe,
    ShowFileComponent,
    UploadFileComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    RouterModule,
    SwiperModule,
    AvatarModule,
    ReactiveFormsModule
  ],
  exports: [
    NavbarComponent,
    ConstructionComponent,
    MainServiceComponent,
    AboutMeComponent,
    SpecialityComponent,
    LearningComponent,
    TitleSectionComponent,
    TestimonyComponent,
    OurHistoryComponent,
    LearningUsComponent,
    SidenavComponent,
    SearchPipe,
    FilterPipe,
    RouteSectionComponent,
    AdminTitleSectionComponent,
    NavbarAdminComponent,
    EmptyStateComponent,
    NetworkSocialComponent,
    CoursePriceComponent,
    TextDecorationDirective,
    CourseExistPipe,
    ShoppingListComponent,
    TotalPricePipe,
    UploadFileComponent,
    FooterComponent
  ]
})
export class SharedModule { }
