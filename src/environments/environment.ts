// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'sex-coach-d4944',
    appId: '1:58313854897:web:4837baf26c243d8f33b790',
    storageBucket: 'sex-coach-d4944.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyDs5wuvcyJKufmsVt78JakzwVuittXhdKo',
    authDomain: 'sex-coach-d4944.firebaseapp.com',
    messagingSenderId: '58313854897',
    measurementId: 'G-YYQHHX2J9W',
  },
  production: false,
  apiUrl: 'https://sex-coach-api.herokuapp.com/api/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
